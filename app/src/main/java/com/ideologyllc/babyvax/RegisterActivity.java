package com.ideologyllc.babyvax;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ideologyllc.babyvax.HelperClasses.ServiceHelper;
import com.ideologyllc.babyvax.HelperClasses.SessionHelper;

public class RegisterActivity extends AppCompatActivity {

    Button buttonToLoginScreen, btnLogin;
    EditText etUsername, etPassword, etEmail;
    private ProgressDialog pDialog;
    private SessionHelper session;
    private String username, password, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initialize();

        buttonToLoginScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                username = etUsername.getText().toString();
                password = etPassword.getText().toString();
                email = etEmail.getText().toString();

                String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
                java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
                java.util.regex.Matcher m = p.matcher(email);

                if (!username.isEmpty() && username.length()> 3 && !password.isEmpty() && password.length()> 5 && !email.isEmpty() && m.matches()){
                    addUser(username, password, email);
                }
                else {
                    if (username.length()> 3){
                        Toast.makeText(getApplicationContext(), "username must be 4 characters", Toast.LENGTH_LONG).show();
                    }
                    else if (password.length()> 5){
                        Toast.makeText(getApplicationContext(), "password must be 6 characters", Toast.LENGTH_LONG).show();
                    }
                    else if (email.isEmpty() && m.matches()){
                        Toast.makeText(getApplicationContext(), "enter a valid email", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    public void initialize(){
        buttonToLoginScreen = (Button)findViewById(R.id.buttonToLoginScreen);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etEmail = (EditText) findViewById(R.id.etEmail);
        pDialog = new ProgressDialog(this);
        session = new SessionHelper(getApplicationContext());
    }

    private void addUser(String username, String password, String email){

        String url =  "http://express-agency.somee.com/test/api/babyvaxapi/adduser/"+username+"/"+password+"/"+email+"/";
        pDialog.setMessage("Please wait ...");
        pDialog.show();
        StringRequest strRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                String value = response.toString();
                if (!value.isEmpty()){
                    if (value.equalsIgnoreCase("true")){

                        pDialog.dismiss();
                        session.setLogin(true);
                        Toast.makeText(getApplicationContext(), "Register successful", Toast.LENGTH_LONG).show();
                        String name = "My family";
                        Intent i = new Intent(RegisterActivity.this, MainActivity.class);
                        i.putExtra("name", name);
                        startActivity(i);
                        finish();

                    }else if (value.equalsIgnoreCase("false")){
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "This email has been used", Toast.LENGTH_LONG).show();
                    }
                    else {
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Register failed", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Register failed", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        ServiceHelper.getInstance().addToRequestQueue(strRequest);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(RegisterActivity.this, MainActivity.class);
        i.putExtra("name", "Vaccination schedule");
        startActivity(i);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
    }
}
