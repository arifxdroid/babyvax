package com.ideologyllc.babyvax;

import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.ideologyllc.babyvax.diseaseFragments.Diphtheria;
import com.ideologyllc.babyvax.diseaseFragments.Haemophilus;
import com.ideologyllc.babyvax.diseaseFragments.HepatitisB;
import com.ideologyllc.babyvax.diseaseFragments.Measles;
import com.ideologyllc.babyvax.diseaseFragments.Meningococcal;
import com.ideologyllc.babyvax.diseaseFragments.Mumps;
import com.ideologyllc.babyvax.diseaseFragments.Pertussis;
import com.ideologyllc.babyvax.diseaseFragments.Pneumococcal;
import com.ideologyllc.babyvax.diseaseFragments.Polio;
import com.ideologyllc.babyvax.diseaseFragments.Rubella;
import com.ideologyllc.babyvax.diseaseFragments.Tetanus;
import com.ideologyllc.babyvax.diseaseFragments.Tuberculosis;

public class DiseaseDetails extends AppCompatActivity {

    Diphtheria diphtheria;
    Haemophilus haemophilus;
    HepatitisB hepatitisB;
    Measles measles;
    Meningococcal meningococcal;
    Mumps mumps;
    Pertussis pertussis;
    Pneumococcal pneumococcal;
    Polio polio;
    Rubella rubella;
    Tetanus tetanus;
    Tuberculosis tuberculosis;

    FrameLayout containerDisease;
    private Intent intent;
    private FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disease_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initialize();


        if (intent != null){

            String name = intent.getStringExtra("name");

            if (name.contains("Diphtheria")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, diphtheria, "diphtheria");
                transaction.commit();
            }
            else if (name.contains("Haemophilus")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, haemophilus, "haemophilus");
                transaction.commit();
            }
            else if (name.contains("Hepatitis")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, hepatitisB, "hepatitisB");
                transaction.commit();
            }
            else if (name.contains("Measles")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, measles, "measles");
                transaction.commit();
            }
            else if (name.contains("Meningococcal")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, meningococcal, "meningococcal");
                transaction.commit();
            }
            else if (name.contains("Mumps")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, mumps, "mumps");
                transaction.commit();
            }
            else if (name.contains("Pertussis")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, pertussis, "pertussis");
                transaction.commit();
            }
            else if (name.contains("Pneumococcal")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, pneumococcal, "pneumococcal");
                transaction.commit();
            }
            else if (name.contains("Polio")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, polio, "polio");
                transaction.commit();
            }
            else if (name.contains("Rubella")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, rubella, "rubella");
                transaction.commit();
            }
            else if (name.contains("Tetanus")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, tetanus, "tetanus");
                transaction.commit();
            }
            else if (name.contains("Tuberculosis")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerDisease, tuberculosis, "tuberculosis");
                transaction.commit();
            }
        }

    }

    public void initialize(){

        diphtheria = new Diphtheria();
        haemophilus = new Haemophilus();
        hepatitisB = new HepatitisB();
        measles = new Measles();
        meningococcal = new Meningococcal();
        mumps = new Mumps();
        pertussis = new Pertussis();
        pneumococcal = new Pneumococcal();
        polio = new Polio();
        rubella = new Rubella();
        tetanus = new Tetanus();
        tuberculosis = new Tuberculosis();

        containerDisease = (FrameLayout) findViewById(R.id.containerDisease);

        fragmentManager = getSupportFragmentManager();
        intent = getIntent();
    }

}
