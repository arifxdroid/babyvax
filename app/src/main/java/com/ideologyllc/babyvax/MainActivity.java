package com.ideologyllc.babyvax;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.ideologyllc.babyvax.HelperClasses.SessionHelper;
import com.ideologyllc.babyvax.adapter.ViewPagerAdapter;
import com.ideologyllc.babyvax.database.ChildrenDb;
import com.ideologyllc.babyvax.fragments.ChildrenFragment;
import com.ideologyllc.babyvax.fragments.DiseasesFragment;
import com.ideologyllc.babyvax.fragments.HomeFragment;
import com.ideologyllc.babyvax.fragments.ImmunisationFragment;
import com.ideologyllc.babyvax.template.Immunisation;
import com.ideologyllc.babyvax.template.Pharma;
import com.ideologyllc.babyvax.template.VaccineAge;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    Intent intent;
    private SessionHelper session;
    private boolean checkLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        intent = getIntent();

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        session = new SessionHelper(getApplicationContext());
        checkLogin = session.isLoggedIn();

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new HomeFragment(), "Home");
        viewPagerAdapter.addFragment(new ImmunisationFragment(), "Immunisation");
        viewPagerAdapter.addFragment(new ChildrenFragment(), "Children");
        viewPagerAdapter.addFragment(new DiseasesFragment(), "Diseases");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        if(intent != null){

            String nameFrag = intent.getStringExtra("name");

            if (nameFrag.contains("Vaccination schedule")){
                viewPager.setCurrentItem(0);
            }else if (nameFrag.contains("About the vaccines")){
                viewPager.setCurrentItem(1);
            }else if (nameFrag.contains("About the diseases")){
                viewPager.setCurrentItem(3);
            }else if (nameFrag.contains("My family")){
                viewPager.setCurrentItem(2);
            }
//            else if (nameFrag.contains("Information source")){
//                Intent i = new Intent(MainActivity.this, RegisterActivity.class);
//                startActivity(i);
//                finish();
//            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login_menu, menu);

        if (checkLogin){
            MenuItem itemName = menu.findItem(R.id.action_login);
            itemName.setVisible(false);
            this.invalidateOptionsMenu();
        }else {
            MenuItem itemName = menu.findItem(R.id.action_logOut);
            itemName.setVisible(false);
            this.invalidateOptionsMenu();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_login) {
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
            return true;
        }
        if (id == R.id.action_logOut) {
            if (checkLogin){
                session.setLogin(false);
                Intent i = new Intent(MainActivity.this, MainHome.class);
                startActivity(i);
                finish();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
