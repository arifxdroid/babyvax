package com.ideologyllc.babyvax.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ideologyllc.babyvax.AddChildren;
import com.ideologyllc.babyvax.ChildImmunisationHistory;
import com.ideologyllc.babyvax.HelperClasses.SessionHelper;
import com.ideologyllc.babyvax.LoginActivity;
import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.adapter.ChildView;
import com.ideologyllc.babyvax.database.ChildrenDb;
import com.ideologyllc.babyvax.database.FlagHelperDB;
import com.ideologyllc.babyvax.template.Child;

import java.util.ArrayList;


public class ChildrenFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    RecyclerView child_list_rec;
    ArrayList<Child> childList;
    ChildView adapter;
    int Position;
    private ChildrenDb db;
    private FlagHelperDB flagDB;
    private SessionHelper session;

    public ChildrenFragment() {
    }


    public static ChildrenFragment newInstance(String param1, String param2) {
        ChildrenFragment fragment = new ChildrenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_children, container, false);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        child_list_rec = (RecyclerView) view.findViewById(R.id.child_list_rec);

        db = new ChildrenDb(getContext());
        flagDB = new FlagHelperDB(getContext());
        childList = db.getAllChild();
        adapter = new ChildView(getContext(), childList);
        session = new SessionHelper(getContext());

//        if (!session.isLoggedIn()){
//            Intent i = new Intent(getContext(), LoginActivity.class);
//            startActivity(i);
//            getActivity().finish();
//        }

        child_list_rec.setHasFixedSize(true);
        child_list_rec.setLayoutManager(new LinearLayoutManager(getContext()));
        child_list_rec.setAdapter(adapter);
        adapter.SetOnItemClickListener(new ChildView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Child child = childList.get(position);
                Intent i = new Intent(getContext(), ChildImmunisationHistory.class);
                i.putExtra("id", child.getChildID());
                startActivity(i);
            }
        });

        adapter.SetOnLongClickListener(new ChildView.OnLongClickListener() {
            @Override
            public void onLongPress(View view, int position) {
                Position = position;
            }
        });
        //registerForContextMenu(child_list_rec);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getContext(), AddChildren.class);
                startActivity(i);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().finish();
            }
        });


        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

//    @Override
//    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//        super.onCreateContextMenu(menu, v, menuInfo);
//        getActivity().getMenuInflater().inflate(R.menu.menu_main, menu);
//    }
//
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        final Child child = childList.get(Position);

        int id = item.getItemId();
        if (id == R.id.action_delete){

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
            alertDialog.setTitle("Confirm Delete...");
            alertDialog.setMessage("Are you sure you want delete this?");
            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {

                    childList.remove(Position);
                    child_list_rec.removeViewAt(Position);
                    adapter.notifyItemRemoved(Position);
                    adapter.notifyItemRangeChanged(Position, childList.size());
                    adapter.notifyDataSetChanged();
                    db.deleteChild(child.getChildID());
                    flagDB.deleteFlag(child.getChildID());
                }
            });
            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialog.show();
        }

        return super.onContextItemSelected(item);

    }

}
