package com.ideologyllc.babyvax.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.adapter.NonHseViewAdapter;
import com.ideologyllc.babyvax.database.DbHelperHseNonHse;
import com.ideologyllc.babyvax.template.NonHse;

import java.util.ArrayList;

/**
 * Created by arif on 8/20/15.
 */
public class FragmentNonHse extends Fragment {

    ListView listNonHseFrag;
    DbHelperHseNonHse dbHelper;
    NonHseViewAdapter adapter;
    ArrayList<NonHse> allNonHse;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_non_hse, container, false);

        listNonHseFrag = (ListView)view.findViewById(R.id.listNonHseFrag);
        dbHelper = new DbHelperHseNonHse(getActivity());
        allNonHse = dbHelper.getAllNonHse();
        adapter = new NonHseViewAdapter(getActivity(), allNonHse);
        listNonHseFrag.setAdapter(adapter);
        return view;
    }
}
