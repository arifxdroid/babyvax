package com.ideologyllc.babyvax.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ideologyllc.babyvax.ImmunisationDetails;
import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.adapter.ImmunisationView;
import com.ideologyllc.babyvax.database.DbHelperHseNonHse;
import com.ideologyllc.babyvax.template.VaxItems;

import java.util.ArrayList;


public class ImmunisationFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    RecyclerView immunisation_hse_rec;
    ImmunisationView myAdapter;
    RecyclerView.LayoutManager myLayoutManager;

    private DbHelperHseNonHse dbHelper;
    private ArrayList<VaxItems> all;

    public ImmunisationFragment() {
        // Required empty public constructor
    }


    public static ImmunisationFragment newInstance(String param1, String param2) {
        ImmunisationFragment fragment = new ImmunisationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.fragment_immunisation, container, false);

        dataInitialize();
        immunisation_hse_rec = (RecyclerView)rootView.findViewById(R.id.immunisation_hse_rec);
        immunisation_hse_rec.setHasFixedSize(true);
        immunisation_hse_rec.setLayoutManager(new LinearLayoutManager(getContext()));
        myAdapter = new ImmunisationView(all);
        immunisation_hse_rec.setAdapter(myAdapter);


        myAdapter.SetOnItemClickListener(new ImmunisationView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                VaxItems item = all.get(position);
                String name = item.getName();
                //Toast.makeText(getContext(), name, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getContext(), ImmunisationDetails.class);
                i.putExtra("name", name);
                startActivity(i);

            }
        });

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void dataInitialize(){

        all = new ArrayList<>();
        all.add(new VaxItems("BCG", "At Birth", "is recommended within the first month of life."));
        all.add(new VaxItems("6 in 1", "At 2, 4 and 6 months of age", "6 in 1 vaccine protects against six diseases"));
        all.add(new VaxItems("PCV", "At 2, 6 and 12 months of age", "PCV vaccine to protect against Pneumococcal disease"));
        all.add(new VaxItems("MenC", "At 4 and 13 months of life", "MenC vaccine protect against meningococcal C disease"));
        all.add(new VaxItems("MMR", "At 12 month of life", "MMR vaccine protect against measles, mumps and rubella"));
        all.add(new VaxItems("Hib", "At 13 month of life", "Protect against Haemophilus influenzae b"));
    }
}
