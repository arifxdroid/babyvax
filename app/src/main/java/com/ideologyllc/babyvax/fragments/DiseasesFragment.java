package com.ideologyllc.babyvax.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ideologyllc.babyvax.DiseaseDetails;
import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.adapter.DiseasesView;
import com.ideologyllc.babyvax.template.DiseasesItem;

import java.util.ArrayList;

public class DiseasesFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    ArrayList<DiseasesItem> items;
    RecyclerView disease_rec;
    DiseasesView adapter;

    public DiseasesFragment() {
    }

    public static DiseasesFragment newInstance(String param1, String param2) {
        DiseasesFragment fragment = new DiseasesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_diseases, container, false);
        initialize(view);
        disease_rec.setHasFixedSize(true);
        disease_rec.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapter = new DiseasesView(getContext(), items);
        disease_rec.setAdapter(adapter);
        adapter.SetOnItemClickListener(new DiseasesView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String name = items.get(position).getName();
                Intent i = new Intent(getContext(), DiseaseDetails.class);
                i.putExtra("name", name);
                startActivity(i);
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void initialize(View view){

        items = new DiseasesItem().init();
        disease_rec = (RecyclerView) view.findViewById(R.id.disease_rec);
    }
}
