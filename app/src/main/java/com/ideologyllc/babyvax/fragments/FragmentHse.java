package com.ideologyllc.babyvax.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.adapter.HseViewAdapter;
import com.ideologyllc.babyvax.database.DbHelperHseNonHse;
import com.ideologyllc.babyvax.template.Hse;

import java.util.ArrayList;

/**
 * Created by arif on 8/20/15.
 */
public class FragmentHse extends Fragment {

    ListView listHseFrag;
    DbHelperHseNonHse db;
    ArrayList<Hse> allHse;
    HseViewAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_hse, container, false);

        listHseFrag = (ListView)v.findViewById(R.id.listHseFrag);
        db = new DbHelperHseNonHse(getActivity());
        allHse = db.getAllHse();
        adapter = new HseViewAdapter(getActivity(), allHse);
        listHseFrag.setAdapter(adapter);
        return v;
    }
}
