package com.ideologyllc.babyvax.fragments;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.database.DbHelperHseNonHse;
import com.ideologyllc.babyvax.template.Hse;
import com.ideologyllc.babyvax.template.NonHse;


public class HomeFragment extends Fragment {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    TextView txtHseChange, txtNon;
    private DbHelperHseNonHse dbHelper;
    private static boolean isPresent = false;

    private FragmentNonHse fragmentNonHse;
    private FragmentHse fragmentHse;
    private FragmentManager fragmentManager;

    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View viewRoot =  inflater.inflate(R.layout.fragment_home, container, false);

        initialize(viewRoot);
        dbHelper = new DbHelperHseNonHse(getContext());
        fragmentManager = getFragmentManager();

        if (dbHelper.getAllHse().isEmpty()){
            hseDataInsert();
        }


        final android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.container, fragmentHse, "fragmentHse");
        isPresent = true;
        transaction.commit();

        txtNon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction transaction1 = fragmentManager.beginTransaction();

                if (isPresent) {
                    Fragment fragment = fragmentManager.findFragmentByTag("fragmentHse");
                    transaction1.remove(fragment);
                    transaction1.add(R.id.container, fragmentNonHse, "fragmentNonHse");
                    isPresent = false;
                    txtNon.setText("HSE childhood immunisation schedule");
                    txtHseChange.setText("Non HSE available products.");
                    transaction1.commit();
                } else {

                    FragmentTransaction transaction2 = fragmentManager.beginTransaction();
                    Fragment fragment = fragmentManager.findFragmentByTag("fragmentNonHse");
                    transaction2.remove(fragment);
                    transaction2.add(R.id.container, fragmentHse, "fragmentHse");
                    isPresent = true;
                    txtNon.setText("Privately available vaccines");
                    txtHseChange.setText("HSE childhood immunisation schedule.");
                    transaction2.commit();

                }

            }
        });

        return viewRoot;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void initialize(View view){

        txtHseChange = (TextView) view.findViewById(R.id.txtHseChange);
        txtNon = (TextView)view.findViewById(R.id.txtNon);

        fragmentHse = new FragmentHse();
        fragmentNonHse = new FragmentNonHse();
    }

    public void hseDataInsert(){

        Hse atBirth = new Hse(" At Birth", " BCG", " SSI");
        Hse twoMonths = new Hse(" 2 Months", " 6 in 1\n + \n PCV", " GSK \n Pfizer");
        Hse fourMonths = new Hse(" 4 Months", " 6 in 1\n + \n MenC", " GSK \n Novartis GSK");
        Hse sixMonths = new Hse(" 6 Months", " 6 in 1\n + \n PCV \n+\n MenC", " GSK \n Pfizer");
        Hse twelveMonths = new Hse(" 12 Months", " MMR\n + \n PCV", " GSK or \n Sanofi Pasteur \n MSD \n Pfizer");
        Hse thirteenMonths = new Hse(" 13 Months", " MenC\n + \n Hib", " Novartis GSK \n GSK");


        dbHelper.addHse(atBirth);
        dbHelper.addHse(twoMonths);
        dbHelper.addHse(fourMonths);
        dbHelper.addHse(sixMonths);
        dbHelper.addHse(twelveMonths);
        dbHelper.addHse(thirteenMonths);

        NonHse test = new NonHse(" ", " ", " ");
        dbHelper.addNonHse(test);

    }
}
