package com.ideologyllc.babyvax.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.ideologyllc.babyvax.EditImmunisationDetails;
import com.ideologyllc.babyvax.HelperClasses.AgeCalculation;
import com.ideologyllc.babyvax.MainActivity;
import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.adapter.ChildHistoryView;
import com.ideologyllc.babyvax.database.ChildrenDb;
import com.ideologyllc.babyvax.database.FlagHelperDB;
import com.ideologyllc.babyvax.template.Child;
import com.ideologyllc.babyvax.template.ChildImmunisation;
import com.ideologyllc.babyvax.template.ChildStatusFlag;
import com.ideologyllc.babyvax.template.HistoryListTemplate;
import com.ideologyllc.babyvax.template.VaccineAge;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;


public class Govt extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    CircularImageView imageProPic;
    TextView textName, textDue, textAge;
    Intent intent;
    int childID;
    ChildrenDb db;
    FlagHelperDB flagDB;
    Child aChild;
    RecyclerView history_rec;
    ArrayList<HistoryListTemplate> historyListArray;
    ChildHistoryView adapter;
    private String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    int dueCount = 0;

    public Govt() {
    }


    public static Govt newInstance(String param1, String param2) {
        Govt fragment = new Govt();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_govt, container, false);

        db = new ChildrenDb(getContext());
        flagDB = new FlagHelperDB(getContext());
        initialize(view);
        intent = getActivity().getIntent();

        if (intent != null){
            childID = intent.getIntExtra("id", -1);
        }

        if (childID > 0){
            aChild = db.getAChild(childID);
            imageProPic.setImageURI(Uri.parse(aChild.getImageLink()));
            textAge.setText(new AgeCalculation(aChild.getDOB()).getAge());
            textName.setText(aChild.getChildName());

            if (flagDB.getAllFlag().isEmpty()){
                generateChildDetailsHistory();
            }
            else if (flagDB.getFlag(childID) == null){
                generateChildDetailsHistory();
            }
            historyListArray = getHistoryList();
            history_rec.setHasFixedSize(true);
            history_rec.setLayoutManager(new LinearLayoutManager(getContext()));
            adapter = new ChildHistoryView(historyListArray);
            history_rec.setAdapter(adapter);
            textDue.setText(dueCount + " items due");
            adapter.SetOnItemClickListener(new ChildHistoryView.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    int childImmunisationID = historyListArray.get(position).getChildImmunisationID();
                    Bundle b = new Bundle();
                    b.putInt("childImmunisationID", childImmunisationID);
                    b.putInt("id", childID);
                    b.putString("immuName", historyListArray.get(position).getImmunisation());
                    b.putString("givenDate", historyListArray.get(position).getGivenDate());
                    Intent i = new Intent(getContext(), EditImmunisationDetails.class);
                    i.putExtras(b);
                    startActivity(i);
                    getActivity().finish();
                }
            });
        }
        return view;
    }

    public void recyclerViewGenerate(){

        historyListArray = getHistoryList();
        history_rec.setHasFixedSize(true);
        history_rec.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ChildHistoryView(historyListArray);
        history_rec.setAdapter(adapter);
    }

    public void initialize(View view){
        imageProPic = (CircularImageView) view.findViewById(R.id.imageProPic);
        textName = (TextView) view.findViewById(R.id.textName);
        textDue = (TextView) view.findViewById(R.id.textDue);
        textAge = (TextView) view.findViewById(R.id.textAge);
        history_rec = (RecyclerView) view.findViewById(R.id.history_rec);
    }

    public void generateChildDetailsHistory(){

        ArrayList<VaccineAge> allVaccineAge = db.getAllVaccineAge();
        for (int i = 0; i < allVaccineAge.size(); i++){

            Child child = db.getAChild(childID);
            String DOB = child.getDOB();
            int yearDOB = new AgeCalculation(DOB).getYear();
            int monthDOB = new AgeCalculation(DOB).getMonth() + 1;
            int dayDOB = new AgeCalculation(DOB).getDay();
            int dueYear = yearDOB;
            int dueMonth = 0;
            int vaccineAgeID = allVaccineAge.get(i).getId();
            int vAge = db.getOneVaccineAge(vaccineAgeID).getVaccineAge();

            if (monthDOB + vAge > 12){
                int temp = (monthDOB + vAge) - 12;

                if(temp > 12 ){
                    dueMonth += temp - 12;
                    dueYear += 1;
                }
                else if (temp == 0){
                    dueMonth += 1;
                }else dueMonth += temp;
                dueYear += 1;
            }else dueMonth += monthDOB + vAge;

            String due = "Due on "+ dayDOB + "-" + months[dueMonth-1]+ "-" + dueYear;

            ChildImmunisation childImmunisation = new ChildImmunisation(childID, allVaccineAge.get(i).getId(),due);
            db.addChildImmunisation(childImmunisation);
        }

        long added = flagDB.addHistoryFlag(new ChildStatusFlag(childID, "added"));
        //Toast.makeText(getContext(), "Added Flag = "+added, Toast.LENGTH_LONG).show();
    }

    public ArrayList<HistoryListTemplate> getHistoryList(){

        ArrayList<HistoryListTemplate> historyListArray = new ArrayList<>();
        ArrayList<ChildImmunisation> childImmunisationsArray = db.getChildImmunisation(childID);

        for (int i = 0; i < childImmunisationsArray.size(); i++){

            int childImmunisationID = childImmunisationsArray.get(i).getChildImmunisationID();
            int vaccineAgeID = childImmunisationsArray.get(i).getVaccineAgeID();
            int childId = childImmunisationsArray.get(i).getChildID();

            int vAge = db.getOneVaccineAge(vaccineAgeID).getVaccineAge();
            String age;

            if (vAge == 1){
                age = "At birth";
            }else {
                age = String.valueOf(vAge)+" Months";
            }

            VaccineAge vaccineAge = db.getOneVaccineAge(vaccineAgeID);
            String immuName = db.getAImmunisation(vaccineAge.getImmunisationID()).getImmunisationName();
            String givenDate = childImmunisationsArray.get(i).getGivenDate();

            if (givenDate.contains("Due")){
                dueCount++;
            }

            HistoryListTemplate history = new HistoryListTemplate(age,immuName,givenDate,childImmunisationID,childId,vaccineAgeID);
            historyListArray.add(history);
        }
        return historyListArray;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


}
