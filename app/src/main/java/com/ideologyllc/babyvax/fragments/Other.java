package com.ideologyllc.babyvax.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ideologyllc.babyvax.AddAdditionalImmunisation;
import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.adapter.AdditionalView;
import com.ideologyllc.babyvax.database.ChildrenDb;
import com.ideologyllc.babyvax.template.ImmunisationPrivate;

import java.util.ArrayList;


public class Other extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private Intent intent;
    private int childID;
    RecyclerView additional_rec;
    private ChildrenDb db;
    private ArrayList<ImmunisationPrivate> immunisationPrivates;
    private AdditionalView adapter;

    public Other() {
    }


    public static Other newInstance(String param1, String param2) {
        Other fragment = new Other();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_other, container, false);

        intent = getActivity().getIntent();
        db = new ChildrenDb(getContext());
        if (intent != null){
            childID = intent.getIntExtra("id", -1);
        }
        additional_rec = (RecyclerView) view.findViewById(R.id.additional_rec);
        immunisationPrivates = db.getAllImmunisationPrivate(childID);
        additional_rec.setHasFixedSize(true);
        additional_rec.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new AdditionalView(getContext(), immunisationPrivates);
        additional_rec.setAdapter(adapter);

        adapter.SetOnItemClickListener(new AdditionalView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                int immunisationPrivateID = immunisationPrivates.get(position).getId();
                int childId = immunisationPrivates.get(position).getChildID();
                Bundle b = new Bundle();
                b.putInt("immunisationPrivateID", immunisationPrivateID);
                b.putInt("id", childId);
                Intent i = new Intent(getContext(), AddAdditionalImmunisation.class);
                i.putExtras(b);
                startActivity(i);
                getActivity().finish();

            }
        });


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putInt("id", childID);
                Intent i = new Intent(getContext(), AddAdditionalImmunisation.class);
                i.putExtras(b);
                startActivity(i);
                getActivity().finish();
            }
        });
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
