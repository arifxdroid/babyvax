package com.ideologyllc.babyvax.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ideologyllc.babyvax.template.ChildStatusFlag;
import com.ideologyllc.babyvax.template.MyLocation;
import com.ideologyllc.babyvax.template.TermsFlag;

import java.util.ArrayList;

/**
 * Created by ${Arif} on 5/30/2016.
 */
public class FlagHelperDB extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "flagsDB";
    public static final int VERSION = 1;

    public static final String TABLE_CHILD_HISTORY_FLAG = "childHistoryFlag";
    public static final String HISTORY_FLAG_ID = "_historyFlagID";
    public static final String childID = "childID";
    public static final String STATUS = "status";

    public static final String HISTORY_FLAG_TABLE_SQL = "CREATE TABLE "+TABLE_CHILD_HISTORY_FLAG+" ("+HISTORY_FLAG_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+childID+" INTEGER, "+STATUS+" BOOLEAN)";

    public static final String TABLE_TERMS_FLAG = "termsFlag";
    public static final String TERMS_FLAG_ID = "_termsFlagID";
    public static final String TERMS_FLAG_STATUS = "termsFlagStatus";

    public static final String TERMS_FLAG_TABLE_SQL = "CREATE TABLE "+TABLE_TERMS_FLAG+" ("+TERMS_FLAG_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+TERMS_FLAG_STATUS+" TEXT)";


    public static final String TABLE_LOCATION = "myLocation";
    public static final String LOCATION_ID = "_locationID";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String COUNTRY = "country";
    public static final String DEVICE_ID = "deviceID";

    public static final String LOCATION_TABLE_SQL = "CREATE TABLE "+TABLE_LOCATION+" ("+LOCATION_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+LATITUDE+" TEXT, "+LONGITUDE+" TEXT, "+COUNTRY+" TEXT, "+DEVICE_ID+" TEXT)";


    public FlagHelperDB(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(HISTORY_FLAG_TABLE_SQL);
        db.execSQL(TERMS_FLAG_TABLE_SQL);
        db.execSQL(LOCATION_TABLE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long addHistoryFlag(ChildStatusFlag flag){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(childID, flag.getChildId());
        values.put(STATUS, flag.getStatus());

        long added = db.insert(TABLE_CHILD_HISTORY_FLAG, null, values);
        db.close();
        return added;
    }

    public ChildStatusFlag getFlag(int idChild){

        ChildStatusFlag flag = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " +TABLE_CHILD_HISTORY_FLAG+ " where " +childID+ "='" +idChild+ "'", null);

        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(HISTORY_FLAG_ID));
                    String status = cursor.getString(cursor.getColumnIndex(STATUS));

                    flag = new ChildStatusFlag(id, idChild, status);
                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return flag;
    }

    public ArrayList<ChildStatusFlag> getAllFlag(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHILD_HISTORY_FLAG, null, null, null, null, null, null);
        ArrayList<ChildStatusFlag> all = new ArrayList<>();

        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {

                    int id = cursor.getInt(cursor.getColumnIndex(HISTORY_FLAG_ID));
                    int idChild = cursor.getInt(cursor.getColumnIndex(childID));
                    String status = cursor.getString(cursor.getColumnIndex(STATUS));


                    ChildStatusFlag p = new ChildStatusFlag(id, idChild, status);
                    all.add(p);
                }while (cursor.moveToNext());
            }
        }

        db.close();
        cursor.close();
        return all;
    }

    public int deleteFlag(int idChild){

        int delete = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " +TABLE_CHILD_HISTORY_FLAG+ " where " +childID+ "='" +idChild+ "'", null);

        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {
                    delete = db.delete(TABLE_CHILD_HISTORY_FLAG,childID + "=?",new String[]{""+idChild});
                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return delete;
    }

    public long addTermsFlag(String status){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TERMS_FLAG_STATUS, status);
        long added = db.insert(TABLE_TERMS_FLAG, null, values);
        db.close();

        return added;
    }

    public TermsFlag checkTerms(){

        TermsFlag termsFlag = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_TERMS_FLAG, null, null, null, null, null, null);

        if (cursor != null){

            if (cursor.getCount() > 0){
                cursor.moveToFirst();
                do {

                    String status = cursor.getString(cursor.getColumnIndex(TERMS_FLAG_STATUS));
                    int id = cursor.getInt(cursor.getColumnIndex(TERMS_FLAG_ID));
                    termsFlag = new TermsFlag(id, status);

                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return termsFlag;
    }

    public long addLocation(MyLocation myLocation){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(LATITUDE, myLocation.getLatitude());
        values.put(LONGITUDE, myLocation.getLongitude());
        values.put(COUNTRY, myLocation.getCountry());
        values.put(DEVICE_ID, myLocation.getDeviceID());

        long added = db.insert(TABLE_LOCATION, null, values);
        db.close();
        return added;
    }

    public MyLocation checkLocation(){

        MyLocation myLocation = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_LOCATION, null, null, null, null, null, null);

        if (cursor != null){

            if (cursor.getCount() > 0){
                cursor.moveToFirst();
                do {

                    String country = cursor.getString(cursor.getColumnIndex(COUNTRY));
                    String latitude = cursor.getString(cursor.getColumnIndex(LATITUDE));
                    String longitude = cursor.getString(cursor.getColumnIndex(LONGITUDE));
                    String deviceID = cursor.getString(cursor.getColumnIndex(DEVICE_ID));
                    int id = cursor.getInt(cursor.getColumnIndex(LOCATION_ID));
                    myLocation = new MyLocation(id, latitude, longitude, country, deviceID);

                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return myLocation;
    }
}
