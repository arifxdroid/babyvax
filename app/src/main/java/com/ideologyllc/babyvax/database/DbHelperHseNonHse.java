package com.ideologyllc.babyvax.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ideologyllc.babyvax.template.Hse;
import com.ideologyllc.babyvax.template.NonHse;

import java.util.ArrayList;

/**
 * Created by arif on 8/15/15.
 */
public class DbHelperHseNonHse extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "hseNonHseDb";
    public static final int VERSION = 1;

    public static final String TABLE_HSE = "hse_table";
    public static final String ID_HSE = "_ID_hse";
    public static final String AGE_HSE = "age_hse";
    public static final String IMMUNISATION_HSE = "immunisation_hse";
    public static final String MANUFACTURER_HSE = "manufacturer_hse";

    public static final String HSE_TABLE_SQL = "CREATE TABLE "+TABLE_HSE+" ("+ID_HSE+" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "+AGE_HSE+" TEXT, "+IMMUNISATION_HSE+" TEXT, "+MANUFACTURER_HSE+" TEXT)";


    public static final String TABLE_NON_HSE = "non_hse_table";
    public static final String ID_NON_HSE = "_ID_non_hse";
    public static final String AGE_NON_HSE = "age_non_hse";
    public static final String IMMUNISATION_NON_HSE = "immunisation_non_hse";
    public static final String MANUFACTURER_NON_HSE = "manufacturer_non_hse";

    public static final String NON_HSE_TABLE_SQL = "CREATE TABLE "+TABLE_NON_HSE+" ("+ID_NON_HSE+" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "+AGE_NON_HSE+" TEXT, "+IMMUNISATION_NON_HSE+" TEXT, "+MANUFACTURER_NON_HSE+" TEXT)";


    public DbHelperHseNonHse(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(HSE_TABLE_SQL);
        db.execSQL(NON_HSE_TABLE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HSE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NON_HSE);

    }

    public long addHse(Hse hse){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(AGE_HSE, hse.getAge());
        values.put(IMMUNISATION_HSE, hse.getImmunisation());
        values.put(MANUFACTURER_HSE, hse.getManufacturer());

        long added = db.insert(TABLE_HSE,null,values);
        db.close();
        return added;
    }

    public ArrayList<Hse> getAllHse(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_HSE, null, null, null, null, null, null);
        ArrayList<Hse> all = new ArrayList<>();

        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();

                do {
                    int id = cursor.getInt(cursor.getColumnIndex(ID_HSE));
                    String age = cursor.getString(cursor.getColumnIndex(AGE_HSE));
                    String immunisation = cursor.getString(cursor.getColumnIndex(IMMUNISATION_HSE));
                    String manufacturer = cursor.getString(cursor.getColumnIndex(MANUFACTURER_HSE));

                    Hse hse  = new Hse(id, age, immunisation, manufacturer);
                    all.add(hse);
                }while (cursor.moveToNext());
            }
        }

        db.close();
        cursor.close();
        return all;
    }

    public long addNonHse(NonHse nonHse){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(AGE_NON_HSE, nonHse.getAge());
        values.put(IMMUNISATION_NON_HSE, nonHse.getImmunisation());
        values.put(MANUFACTURER_NON_HSE, nonHse.getManufacturer());

        long added = db.insert(TABLE_NON_HSE,null,values);
        db.close();
        return added;
    }

    public ArrayList<NonHse> getAllNonHse(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NON_HSE, null, null, null, null, null, null);
        ArrayList<NonHse> all = new ArrayList<>();

        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();

                do {
                    int id = cursor.getInt(cursor.getColumnIndex(ID_NON_HSE));
                    String age = cursor.getString(cursor.getColumnIndex(AGE_NON_HSE));
                    String immunisation = cursor.getString(cursor.getColumnIndex(IMMUNISATION_NON_HSE));
                    String manufacturer = cursor.getString(cursor.getColumnIndex(MANUFACTURER_NON_HSE));

                    NonHse hse  = new NonHse(id, age, immunisation, manufacturer);
                    all.add(hse);
                }while (cursor.moveToNext());
            }
        }

        db.close();
        cursor.close();
        return all;
    }
}
