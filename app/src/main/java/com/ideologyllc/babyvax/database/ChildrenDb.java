package com.ideologyllc.babyvax.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ideologyllc.babyvax.template.Child;
import com.ideologyllc.babyvax.template.ChildImmunisation;
import com.ideologyllc.babyvax.template.GivenImmunisationHistory;
import com.ideologyllc.babyvax.template.Immunisation;
import com.ideologyllc.babyvax.template.ImmunisationPrivate;
import com.ideologyllc.babyvax.template.Pharma;
import com.ideologyllc.babyvax.template.VaccineAge;

import java.util.ArrayList;

/**
 * Created by ${Arif} on 5/17/2016.
 */

public class ChildrenDb extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "AboutChildren";
    public static final int VERSION = 1;

    public static final String TABLE_VaccineAge = "VaccineAge";
    public static final String VaccineAgeID = "_VaccineAgeID";
    public static final String VaccineAge = "VaccineAge";
    public static final String ImmunisationID = "ImmunisationID";

    public static final String VaccineAge_TABLE_SQL = "CREATE TABLE "+TABLE_VaccineAge+" ("+VaccineAgeID+" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "+VaccineAge+" INTEGER, "+ImmunisationID+" INTEGER)";


    public static final String TABLE_IMMUNISATION = "immunisation_table";
    public static final String IMMUNISATION_ID = "_ImmunisationID";
    public static final String pharmaID = "PharmaID";
    public static final String IMMUNISATION_NAME = "immunisation_name";
    public static final String IMMUNISATION_DESCRIPTION = "immunisation_description";

    public static final String Immunisation_TABLE_SQL = "CREATE TABLE "+TABLE_IMMUNISATION+" ("+IMMUNISATION_ID+" INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, "+ pharmaID +" INTEGER, "+IMMUNISATION_NAME+" TEXT, "+IMMUNISATION_DESCRIPTION+" TEXT)";


    public static final String TABLE_PHARMA = "pharma_table";
    public static final String PHARMA_ID = "_PharmaID";
    public static final String PHARMA_NAME = "pharma_name";
    public static final String PHARMA_DESCRIPTION = "pharma_description";
    public static final String PHARMA_COUNTRY = "pharma_country";

    public static final String PHARMA_TABLE_SQL = "CREATE TABLE "+TABLE_PHARMA+" ("+PHARMA_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+PHARMA_NAME+" TEXT, "+PHARMA_DESCRIPTION+" TEXT, "+PHARMA_COUNTRY+" TEXT)";


    public static final String TABLE_CHILD = "child_table";
    public static final String CHILD_ID = "_ChildID";
    public static final String CHILD_NAME = "child_name";
    public static final String CHILD_IMAGE_LINK = "child_image_link";
    public static final String CHILD_DOB = "child_dob";

    public static final String CHILD_TABLE_SQL = "CREATE TABLE "+TABLE_CHILD+" ("+CHILD_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+CHILD_NAME+" TEXT, "+CHILD_DOB+" TEXT, "+CHILD_IMAGE_LINK+" TEXT)";


    public static final String TABLE_CHILD_IMMUNISATION = "child_immunisation_table";
    public static final String CHILD_IMMUNISATION_ID = "_ChildImmunisationID";
    public static final String childID = "ChildID";
    public static final String vaccineAgeID = "ImmunisationID";
    public static final String GIVEN_DATE = "given_date";

    public static final String CHILD_IMMUNISATION_TABLE_SQL = "CREATE TABLE "+TABLE_CHILD_IMMUNISATION+" ("+CHILD_IMMUNISATION_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+childID+" INTEGER, "+ vaccineAgeID +" INTEGER, "+GIVEN_DATE+" TEXT)";


    public static final String TABLE_GIVEN_IMMUNISATION_HISTORY = "given_immunisation_history_table";
    public static final String HISTORY_IMMUNISATION_ID = "_givenImmunisationHistoryID";
    public static final String CHILD_Immunisation_ID = "CHILD_Immunisation_ID";
    public static final String pharma_ID = "pharma_ID";
    public static final String HSE_NUMBER = "hseNumber";
    public static final String DOCTOR_NAME = "doctorName";
    public static final String LOT = "lot";

    public static final String GIVEN_IMMUNISATION_HISTORY_TABLE_SQL = "CREATE TABLE "+ TABLE_GIVEN_IMMUNISATION_HISTORY +" ("+HISTORY_IMMUNISATION_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+CHILD_Immunisation_ID+" INTEGER, "+pharma_ID+" INTEGER, "+HSE_NUMBER+" TEXT, "+DOCTOR_NAME+" TEXT, "+LOT+" TEXT)";


    public static final String TABLE_IMMUNISATION_PRIVATE = "immunisationPrivate_table";
    public static final String IMMUNISATION_PRIVATE_ID = "_ImmunisationPrivateID";
    public static final String ChildID = "ChildID";
    public static final String IMMUNISATION_PRIVATE_NAME = "immunisationPrivate_name";
    public static final String IMMUNISATION_PRIVATE_DESCRIPTION = "immunisationPrivate_description";
    public static final String IMMUNISATION_PRIVATE_PHARMA_NAME = "immunisationPrivate_pharmaName";
    public static final String IMMUNISATION_PRIVATE_GIVEN_DATE = "immunisationPrivate_givenDate";
    public static final String IMMUNISATION_PRIVATE_DOCTOR_NAME = "immunisationPrivate_doctorName";

    public static final String ImmunisationPrivate_TABLE_SQL = "CREATE TABLE "+TABLE_IMMUNISATION_PRIVATE+" ("+IMMUNISATION_PRIVATE_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+ChildID+" INTEGER, "+IMMUNISATION_PRIVATE_NAME+" TEXT, "+IMMUNISATION_PRIVATE_DESCRIPTION+" TEXT, "+IMMUNISATION_PRIVATE_PHARMA_NAME+" TEXT, "+IMMUNISATION_PRIVATE_GIVEN_DATE+" TEXT, "+IMMUNISATION_PRIVATE_DOCTOR_NAME+" TEXT)";


    public ChildrenDb(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(VaccineAge_TABLE_SQL);
        db.execSQL(Immunisation_TABLE_SQL);
        db.execSQL(PHARMA_TABLE_SQL);
        db.execSQL(CHILD_TABLE_SQL);
        db.execSQL(CHILD_IMMUNISATION_TABLE_SQL);
        db.execSQL(GIVEN_IMMUNISATION_HISTORY_TABLE_SQL);
        db.execSQL(ImmunisationPrivate_TABLE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VaccineAge);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMMUNISATION);

    }

    public long addVaccineAge(VaccineAge vaccineAge){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(VaccineAge, vaccineAge.getVaccineAge());
        values.put(ImmunisationID, vaccineAge.getImmunisationID());

        long added = db.insert(TABLE_VaccineAge,null,values);
        db.close();
        return added;
    }

    public ArrayList<VaccineAge> getAllVaccineAge(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_VaccineAge, null, null, null, null, null, null);
        ArrayList<VaccineAge> all = new ArrayList<>();

        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();

                do {
                    int id = cursor.getInt(cursor.getColumnIndex(VaccineAgeID));
                    int age = cursor.getInt(cursor.getColumnIndex(VaccineAge));
                    int immunisationId = cursor.getInt(cursor.getColumnIndex(ImmunisationID));

                    VaccineAge vaccineAge = new VaccineAge(id,immunisationId, age);
                    all.add(vaccineAge);
                }while (cursor.moveToNext());
            }
        }

        db.close();
        cursor.close();
        return all;
    }

    public VaccineAge getOneVaccineAge(int id){

        VaccineAge vaccineAge = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_VaccineAge + " where " + VaccineAgeID + "='" + id + "'", null);
        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {
                    int immuID = cursor.getInt(cursor.getColumnIndex(ImmunisationID));
                    int age = cursor.getInt(cursor.getColumnIndex(VaccineAge));
                    vaccineAge = new VaccineAge(id, immuID, age);
                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return vaccineAge;
    }

    public long addImmunisation(Immunisation immunisation){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(pharmaID, immunisation.getPharmaID());
        values.put(IMMUNISATION_NAME, immunisation.getImmunisationName());
        values.put(IMMUNISATION_DESCRIPTION, immunisation.getImmunisationDescription());

        long added = db.insert(TABLE_IMMUNISATION,null,values);
        db.close();
        return added;
    }

    public long addImmunisationWithoutPharma(Immunisation immunisation){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(IMMUNISATION_NAME, immunisation.getImmunisationName());
        values.put(IMMUNISATION_DESCRIPTION, immunisation.getImmunisationDescription());

        long added = db.insert(TABLE_IMMUNISATION,null,values);
        db.close();
        return added;
    }

    public int updateImmunisationWithPharma(int immunisationId, int pharmaId){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(pharmaID, pharmaId);
        int updated = db.update(TABLE_IMMUNISATION, values, IMMUNISATION_ID + "=?", new String[]{"" + immunisationId});
        db.close();
        return updated;
    }

    public ArrayList<Immunisation> getAllImmunisation(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_IMMUNISATION, null, null, null, null, null, null);
        ArrayList<Immunisation> all = new ArrayList<>();

        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();

                do {
                    int immunisationId = cursor.getInt(cursor.getColumnIndex(IMMUNISATION_ID));
                    int pharmaId = cursor.getInt(cursor.getColumnIndex(pharmaID));
                    String immunisationName = cursor.getString(cursor.getColumnIndex(IMMUNISATION_NAME));
                    String immunisationDescription = cursor.getString(cursor.getColumnIndex(IMMUNISATION_DESCRIPTION));

                    Immunisation immunisation  = new Immunisation(immunisationId, pharmaId, immunisationName, immunisationDescription);
                    all.add(immunisation);
                }while (cursor.moveToNext());
            }
        }

        db.close();
        cursor.close();
        return all;
    }

    public Immunisation getAImmunisation(int id){

        Immunisation immunisation = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_IMMUNISATION + " where " + IMMUNISATION_ID + "='" + id + "'", null);
        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {
                    int pharma_id = cursor.getInt(cursor.getColumnIndex(pharmaID));
                    String immuName = cursor.getString(cursor.getColumnIndex(IMMUNISATION_NAME));
                    String immuDescription = cursor.getString(cursor.getColumnIndex(IMMUNISATION_DESCRIPTION));
                    immunisation = new Immunisation(id, pharma_id, immuName, immuDescription);
                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return immunisation;
    }

    public long addPharma(Pharma pharma){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(PHARMA_NAME, pharma.getPharmaName());
        values.put(PHARMA_DESCRIPTION, pharma.getPharmaDescription());
        values.put(PHARMA_COUNTRY, pharma.getPharmaCountry());

        long added = db.insert(TABLE_PHARMA, null, values);
        db.close();
        return added;
    }

    public ArrayList<Pharma> getAllPharma(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_PHARMA, null, null, null, null, null, null);
        ArrayList<Pharma> all = new ArrayList<>();

        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {

                    int pharmaID = cursor.getInt(cursor.getColumnIndex(PHARMA_ID));
                    String pharmaName = cursor.getString(cursor.getColumnIndex(PHARMA_NAME));
                    String description = cursor.getString(cursor.getColumnIndex(PHARMA_DESCRIPTION));
                    String country = cursor.getString(cursor.getColumnIndex(PHARMA_COUNTRY));

                    Pharma p = new Pharma(pharmaID, pharmaName, description, country);
                    all.add(p);
                }while (cursor.moveToNext());
            }
        }

        db.close();
        cursor.close();
        return all;
    }

    public Pharma getAPharma(int pharma_id){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_PHARMA + " where " + PHARMA_ID + "='" + pharma_id + "'", null);
        Pharma p = null;

        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {

                    String pharmaName = cursor.getString(cursor.getColumnIndex(PHARMA_NAME));
                    String description = cursor.getString(cursor.getColumnIndex(PHARMA_DESCRIPTION));
                    String country = cursor.getString(cursor.getColumnIndex(PHARMA_COUNTRY));

                    p = new Pharma(pharma_id, pharmaName, description, country);
                }while (cursor.moveToNext());
            }
        }

        db.close();
        cursor.close();
        return p;
    }

    public long addChild(Child child){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(CHILD_NAME, child.getChildName());
        values.put(CHILD_DOB, child.getDOB());
        values.put(CHILD_IMAGE_LINK, child.getImageLink());

        long added = db.insert(TABLE_CHILD, null, values);
        db.close();
        return added;
    }

    public ArrayList<Child> getAllChild(){

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_CHILD, null, null, null, null, null, null);
        ArrayList<Child> all = new ArrayList<>();

        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {

                    int childID = cursor.getInt(cursor.getColumnIndex(CHILD_ID));
                    String name = cursor.getString(cursor.getColumnIndex(CHILD_NAME));
                    String DOB = cursor.getString(cursor.getColumnIndex(CHILD_DOB));
                    String imageLink = cursor.getString(cursor.getColumnIndex(CHILD_IMAGE_LINK));

                    Child c = new Child(childID, name, DOB, imageLink);
                    all.add(c);
                }while (cursor.moveToNext());
            }
        }

        db.close();
        cursor.close();
        return all;
    }

    public Child getAChild(int childID){

        Child aChild = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_CHILD + " where " + CHILD_ID + "='" + childID + "'", null);

        if (cursor != null){

            if (cursor.getCount() > 0){
                cursor.moveToFirst();
                do {
                    String name = cursor.getString(cursor.getColumnIndex(CHILD_NAME));
                    String imageLink = cursor.getString(cursor.getColumnIndex(CHILD_IMAGE_LINK));
                    String DOB = cursor.getString(cursor.getColumnIndex(CHILD_DOB));
                    aChild = new Child(childID, name, DOB, imageLink);
                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return aChild;
    }

    public int deleteChild(int childID){

        SQLiteDatabase db = this.getWritableDatabase();
        deleteChildImmunisation(childID);
        deleteImmunisationPrivate(childID);
        int deleted = db.delete(TABLE_CHILD,CHILD_ID + "=?", new String[]{"" + childID});
        db.close();
        return deleted;
    }

    public long addChildImmunisation(ChildImmunisation ci){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(childID, ci.getChildID());
        values.put(vaccineAgeID, ci.getVaccineAgeID());
        values.put(GIVEN_DATE, ci.getGivenDate());

        long result = db.insert(TABLE_CHILD_IMMUNISATION, null, values);
        db.close();
        return result;
    }


    public ArrayList<ChildImmunisation> getChildImmunisation(int idChild){

        ArrayList<ChildImmunisation> list = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_CHILD_IMMUNISATION + " where " + childID + "='" + idChild + "'", null);
        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(CHILD_IMMUNISATION_ID));
                    int child_id = cursor.getInt(cursor.getColumnIndex(childID));
                    int vaccine_ageID = cursor.getInt(cursor.getColumnIndex(vaccineAgeID));
                    String givenDate = cursor.getString(cursor.getColumnIndex(GIVEN_DATE));
                    ChildImmunisation childImmunisation = new ChildImmunisation(id, child_id, vaccine_ageID, givenDate);
                    list.add(childImmunisation);
                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return list;
    }

    public ChildImmunisation getAChildImmunisation(int childImmuID){

        ChildImmunisation childImmunisation  = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_CHILD_IMMUNISATION + " where " + CHILD_IMMUNISATION_ID + "='" + childImmuID + "'", null);
        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(CHILD_IMMUNISATION_ID));
                    int child_id = cursor.getInt(cursor.getColumnIndex(childID));
                    int vaccine_ageID = cursor.getInt(cursor.getColumnIndex(vaccineAgeID));
                    String givenDate = cursor.getString(cursor.getColumnIndex(GIVEN_DATE));
                    childImmunisation = new ChildImmunisation(id, child_id, vaccine_ageID, givenDate);
                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return childImmunisation;
    }


    public int deleteChildImmunisation(int idChild){

        int deleted = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_CHILD_IMMUNISATION + " where " + childID + "='" + idChild + "'", null);
        if (cursor != null){

            if (cursor.getCount() > 0){

                cursor.moveToFirst();
                do {

                    int id = cursor.getInt(cursor.getColumnIndex(CHILD_IMMUNISATION_ID));
                    deleteAGivenImmunisationHistory(id);
                    deleted = db.delete(TABLE_CHILD_IMMUNISATION, childID + "=?", new String[]{""+idChild});

                }while (cursor.moveToNext());
            }
        }
        //db.close();
        cursor.close();
        return deleted;
    }

    public int updateChildImmunisationGivenDate(int childImmunisationId, String givenDate){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(GIVEN_DATE, givenDate);
        int updated = db.update(TABLE_CHILD_IMMUNISATION, values, CHILD_IMMUNISATION_ID + "=?", new String[]{"" + childImmunisationId});
        db.close();
        return updated;
    }

    public long addGivenImmunisationHistory(GivenImmunisationHistory history){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(CHILD_Immunisation_ID, history.getChildImmunisationID());
        values.put(pharma_ID, history.getPharmaID());
        values.put(HSE_NUMBER, history.getHseNumber());
        values.put(DOCTOR_NAME, history.getDoctorName());
        values.put(LOT, history.getLot());

        long added = db.insert(TABLE_GIVEN_IMMUNISATION_HISTORY, null, values);
        db.close();
        return added;
    }

    public long updateGivenImmunisationHistory(GivenImmunisationHistory history){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(CHILD_Immunisation_ID, history.getChildImmunisationID());
        values.put(pharma_ID, history.getPharmaID());
        values.put(HSE_NUMBER, history.getHseNumber());
        values.put(DOCTOR_NAME, history.getDoctorName());
        values.put(LOT, history.getLot());

        long added = db.update(TABLE_GIVEN_IMMUNISATION_HISTORY, values, HISTORY_IMMUNISATION_ID + "=?", new String[]{"" + history.getId()});
        db.close();
        return added;
    }

    public GivenImmunisationHistory getAGivenImmunisationHistory(int childImmunisationID){

        GivenImmunisationHistory history = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_GIVEN_IMMUNISATION_HISTORY + " where " +CHILD_Immunisation_ID+ "='" + childImmunisationID + "'", null);

        if (cursor != null){

            if (cursor.getCount() > 0){
                cursor.moveToFirst();
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(HISTORY_IMMUNISATION_ID));
                    int pharmaId = cursor.getInt(cursor.getColumnIndex(pharma_ID));
                    String doctorName = cursor.getString(cursor.getColumnIndex(DOCTOR_NAME));
                    String hseNum = cursor.getString(cursor.getColumnIndex(HSE_NUMBER));
                    String lot = cursor.getString(cursor.getColumnIndex(LOT));
                    history = new GivenImmunisationHistory(id, childImmunisationID, pharmaId, hseNum, doctorName, lot);

                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return history;
    }

    public int deleteAGivenImmunisationHistory(int childImmunisationID){

        int deleted = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_GIVEN_IMMUNISATION_HISTORY + " where " +CHILD_Immunisation_ID+ "='" + childImmunisationID + "'", null);

        if (cursor != null){

            if (cursor.getCount() > 0){
                cursor.moveToFirst();
                do {

                    deleted = db.delete(TABLE_GIVEN_IMMUNISATION_HISTORY,CHILD_Immunisation_ID + "=?",new String[]{""+childImmunisationID});
                }while (cursor.moveToNext());
            }
        }
        //db.close();
        cursor.close();
        return deleted;
    }

    public long addImmunisationPrivate(ImmunisationPrivate immunisationPrivate){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(ChildID, immunisationPrivate.getChildID());
        values.put(IMMUNISATION_PRIVATE_NAME, immunisationPrivate.getName());
        values.put(IMMUNISATION_PRIVATE_DESCRIPTION, immunisationPrivate.getDescription());
        values.put(IMMUNISATION_PRIVATE_PHARMA_NAME, immunisationPrivate.getPharmaName());
        values.put(IMMUNISATION_PRIVATE_GIVEN_DATE, immunisationPrivate.getGivenDate());
        values.put(IMMUNISATION_PRIVATE_DOCTOR_NAME, immunisationPrivate.getDoctorName());

        long added = db.insert(TABLE_IMMUNISATION_PRIVATE,null,values);
        db.close();
        return added;
    }

    public long updateImmunisationPrivate(ImmunisationPrivate immunisationPrivate){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(IMMUNISATION_PRIVATE_NAME, immunisationPrivate.getName());
        values.put(IMMUNISATION_PRIVATE_DESCRIPTION, immunisationPrivate.getDescription());
        values.put(IMMUNISATION_PRIVATE_PHARMA_NAME, immunisationPrivate.getPharmaName());
        values.put(IMMUNISATION_PRIVATE_GIVEN_DATE, immunisationPrivate.getGivenDate());
        values.put(IMMUNISATION_PRIVATE_DOCTOR_NAME, immunisationPrivate.getDoctorName());

        long updated = db.update(TABLE_IMMUNISATION_PRIVATE, values, IMMUNISATION_PRIVATE_ID + "=?", new String[]{"" + immunisationPrivate.getId()});
        db.close();
        return updated;
    }

    public ImmunisationPrivate getAImmunisationPrivate(int immunisationPrivateID){

        ImmunisationPrivate immunisationPrivate = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_IMMUNISATION_PRIVATE + " where " +IMMUNISATION_PRIVATE_ID+ "='" + immunisationPrivateID + "'", null);

        if (cursor != null){

            if (cursor.getCount() > 0){
                cursor.moveToFirst();
                do {
                    int childId = cursor.getInt(cursor.getColumnIndex(ChildID));
                    String name = cursor.getString(cursor.getColumnIndex(IMMUNISATION_PRIVATE_NAME));
                    String description = cursor.getString(cursor.getColumnIndex(IMMUNISATION_PRIVATE_DESCRIPTION));
                    String pharmaName = cursor.getString(cursor.getColumnIndex(IMMUNISATION_PRIVATE_PHARMA_NAME));
                    String givenDate = cursor.getString(cursor.getColumnIndex(IMMUNISATION_PRIVATE_GIVEN_DATE));
                    String doctorName = cursor.getString(cursor.getColumnIndex(IMMUNISATION_PRIVATE_DOCTOR_NAME));
                    immunisationPrivate = new ImmunisationPrivate(immunisationPrivateID, childId, name, description, pharmaName, givenDate,doctorName);

                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return immunisationPrivate;
    }

    public ArrayList<ImmunisationPrivate> getAllImmunisationPrivate(int childID){

        ArrayList<ImmunisationPrivate> immunisationPrivateList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_IMMUNISATION_PRIVATE + " where " +ChildID+ "='" + childID + "'", null);

        if (cursor != null){

            if (cursor.getCount() > 0){
                cursor.moveToFirst();
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(IMMUNISATION_PRIVATE_ID));
                    int childId = cursor.getInt(cursor.getColumnIndex(ChildID));
                    String name = cursor.getString(cursor.getColumnIndex(IMMUNISATION_PRIVATE_NAME));
                    String description = cursor.getString(cursor.getColumnIndex(IMMUNISATION_PRIVATE_DESCRIPTION));
                    String pharmaName = cursor.getString(cursor.getColumnIndex(IMMUNISATION_PRIVATE_PHARMA_NAME));
                    String givenDate = cursor.getString(cursor.getColumnIndex(IMMUNISATION_PRIVATE_GIVEN_DATE));
                    String doctorName = cursor.getString(cursor.getColumnIndex(IMMUNISATION_PRIVATE_DOCTOR_NAME));

                    ImmunisationPrivate immunisationPrivate = new ImmunisationPrivate(id, childId, name, description, pharmaName, givenDate,doctorName);
                    immunisationPrivateList.add(immunisationPrivate);

                }while (cursor.moveToNext());
            }
        }
        db.close();
        cursor.close();
        return immunisationPrivateList;
    }

    public int deleteImmunisationPrivate(int childID){

        int deleted = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + TABLE_IMMUNISATION_PRIVATE + " where " +ChildID+ "='" + childID + "'", null);

        if (cursor != null){

            if (cursor.getCount() > 0){
                cursor.moveToFirst();
                do {
                    deleted = db.delete(TABLE_IMMUNISATION_PRIVATE,ChildID + "=?",new String[]{""+childID});
                }while (cursor.moveToNext());
            }
        }
        //db.close();
        cursor.close();
        return deleted;
    }
}
