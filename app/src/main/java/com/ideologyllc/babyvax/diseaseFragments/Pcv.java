package com.ideologyllc.babyvax.diseaseFragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ideologyllc.babyvax.R;

/**
 * Created by arif on 9/3/15.
 */
public class Pcv extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_pcv, container, false);
        return v;
    }
}
