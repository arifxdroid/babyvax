package com.ideologyllc.babyvax.diseaseFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;


import com.ideologyllc.babyvax.R;

/**
 * Created by arif on 8/31/15.
 */
public class Diphtheria extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_diphtheria, container, false);
        return v;
    }
}
