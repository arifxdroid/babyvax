package com.ideologyllc.babyvax;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.Toast;

import com.ideologyllc.babyvax.adapter.HomeAdapter;
import com.ideologyllc.babyvax.adapter.HomeView;
import com.ideologyllc.babyvax.database.ChildrenDb;
import com.ideologyllc.babyvax.database.FlagHelperDB;
import com.ideologyllc.babyvax.fragments.HomeFragment;
import com.ideologyllc.babyvax.template.HomeGrid;
import com.ideologyllc.babyvax.template.Immunisation;
import com.ideologyllc.babyvax.template.Pharma;
import com.ideologyllc.babyvax.template.TermsFlag;
import com.ideologyllc.babyvax.template.VaccineAge;

import java.util.ArrayList;

public class MainHome extends AppCompatActivity {

    ArrayList<HomeGrid> gridList;
    RecyclerView home_rec;
    HomeView mAdapter;
    static boolean agreementFlag;
    private ChildrenDb dbChildren;
    FlagHelperDB flagHelperDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        dbChildren = new ChildrenDb(getApplicationContext());
        flagHelperDB = new FlagHelperDB(getApplicationContext());

        if (dbChildren.getAllVaccineAge().isEmpty()){
            addData();
        }

        AgreementLicence agreement = new AgreementLicence();
        if (flagHelperDB.checkTerms() == null){
            agreement.show(getSupportFragmentManager(), "Dialog");
        }

        initialize();

        home_rec.setHasFixedSize(true);
        home_rec.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        mAdapter = new HomeView(getApplicationContext(),gridList);
        home_rec.setAdapter(mAdapter);
        mAdapter.SetOnItemClickListener(new HomeView.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                String name = gridList.get(position).getName();
                switch (name){
                    case "Vaccine and autism":
                        Intent i = new Intent(MainHome.this, MainContainer.class);
                        i.putExtra("name", name);
                        startActivity(i);
                        break;

                    case "About the app":
                        Intent j = new Intent(MainHome.this, MainContainer.class);
                        j.putExtra("name", name);
                        startActivity(j);
                        break;

                    default:
                        Intent k = new Intent(MainHome.this, MainActivity.class);
                        k.putExtra("name", name);
                        startActivity(k);
                        break;
                }

            }
        });

    }

    private void initialize(){

        gridList = new HomeGrid().homeInit();
        home_rec = (RecyclerView)findViewById(R.id.home_rec);
    }


    public static class AgreementLicence extends DialogFragment {
        CheckBox checkBoxShow;
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.terms_condition, null);
            checkBoxShow = (CheckBox) view.findViewById(R.id.checkBoxShow);
            builder.setView(view);
            builder.setPositiveButton("I Agree & Proceed", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    if(checkBoxShow.isChecked()){
                        agreementFlag = true;
                        FlagHelperDB db  = new FlagHelperDB(getContext());
                        db.addTermsFlag("true");
                    }
                    dialogInterface.dismiss();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getActivity().finish();
                }
            });
            AlertDialog alert = builder.create();
            setCancelable(false);
            alert.setCanceledOnTouchOutside(false);


            return alert;
        }

    }

    public void addData(){

        Pharma pharma1 = new Pharma("SSI","","");
        Pharma pharma2 = new Pharma("GSK","","");
        Pharma pharma3 = new Pharma("Pfizer","","");
        Pharma pharma4 = new Pharma("Novartis GSK","","");
        Pharma pharma5 = new Pharma("Sanofi Pasteur","","");
        Pharma pharma6 = new Pharma("MSD","","");

        dbChildren.addPharma(pharma1);
        dbChildren.addPharma(pharma2);
        dbChildren.addPharma(pharma3);
        dbChildren.addPharma(pharma4);
        dbChildren.addPharma(pharma5);
        dbChildren.addPharma(pharma6);

        Immunisation immunisation1 = new Immunisation("", "BCG");
        Immunisation immunisation2 = new Immunisation("","6 in 1");
        Immunisation immunisation3 = new Immunisation("", "PCV");
        Immunisation immunisation4 = new Immunisation("", "MenC");
        Immunisation immunisation5 = new Immunisation("", "MMR");
        Immunisation immunisation6 = new Immunisation("", "Hib");

        dbChildren.addImmunisationWithoutPharma(immunisation1);
        dbChildren.addImmunisationWithoutPharma(immunisation2);
        dbChildren.addImmunisationWithoutPharma(immunisation3);
        dbChildren.addImmunisationWithoutPharma(immunisation4);
        dbChildren.addImmunisationWithoutPharma(immunisation5);
        dbChildren.addImmunisationWithoutPharma(immunisation6);

        int bcgID = 0;
        int sixInOneId = 0;
        int pcvID = 0;
        int mencId = 0;
        int mmrID = 0;
        int hibID = 0;

        ArrayList<Immunisation> allImmunisation = dbChildren.getAllImmunisation();
        for (int i = 0; i < allImmunisation.size(); i++){

            String name = allImmunisation.get(i).getImmunisationName();
            switch (name){

                case "BCG":
                    bcgID = allImmunisation.get(i).getImmunisationID();
                case "6 in 1":
                    sixInOneId = allImmunisation.get(i).getImmunisationID();
                case "PCV":
                    pcvID = allImmunisation.get(i).getImmunisationID();
                case "MenC":
                    mencId = allImmunisation.get(i).getImmunisationID();
                case "MMR":
                    mmrID = allImmunisation.get(i).getImmunisationID();
                case "Hib":
                    hibID = allImmunisation.get(i).getImmunisationID();
            }
        }
        VaccineAge vaccineAge1 = new VaccineAge(bcgID, 1);
        VaccineAge vaccineAge2 = new VaccineAge(sixInOneId, 2);
        VaccineAge vaccineAge3 = new VaccineAge(pcvID, 2);
        VaccineAge vaccineAge4 = new VaccineAge(sixInOneId, 4);
        VaccineAge vaccineAge5 = new VaccineAge(mencId, 4);
        VaccineAge vaccineAge6 = new VaccineAge(sixInOneId, 6);
        VaccineAge vaccineAge7 = new VaccineAge(pcvID, 6);
        VaccineAge vaccineAge8 = new VaccineAge(mencId, 6);
        VaccineAge vaccineAge9 = new VaccineAge(mmrID, 12);
        VaccineAge vaccineAge10 = new VaccineAge(pcvID, 12);
        VaccineAge vaccineAge11 = new VaccineAge(mencId, 13);
        VaccineAge vaccineAge12 = new VaccineAge(hibID, 13);

        dbChildren.addVaccineAge(vaccineAge1);
        dbChildren.addVaccineAge(vaccineAge2);
        dbChildren.addVaccineAge(vaccineAge3);
        dbChildren.addVaccineAge(vaccineAge4);
        dbChildren.addVaccineAge(vaccineAge5);
        dbChildren.addVaccineAge(vaccineAge6);
        dbChildren.addVaccineAge(vaccineAge7);
        dbChildren.addVaccineAge(vaccineAge8);
        dbChildren.addVaccineAge(vaccineAge9);
        dbChildren.addVaccineAge(vaccineAge10);
        dbChildren.addVaccineAge(vaccineAge11);
        long added = dbChildren.addVaccineAge(vaccineAge12);

        //Toast.makeText(getApplicationContext(), "Vaccine Age Flag: "+added, Toast.LENGTH_LONG).show();
    }

}
