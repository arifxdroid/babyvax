package com.ideologyllc.babyvax.template;

/**
 * Created by Arif on 12/4/2016.
 */

public class MyLocation {
    private int id;
    private String latitude;
    private String longitude;
    private String country;
    private String deviceID;

    public MyLocation(int id, String latitude, String longitude, String country, String deviceID) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.country = country;
        this.deviceID = deviceID;
    }

    public MyLocation(String latitude, String longitude, String country, String deviceID) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.country = country;
        this.deviceID = deviceID;
    }

    public int getId() {
        return id;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getCountry() {
        return country;
    }

    public String getDeviceID() {
        return deviceID;
    }
}
