package com.ideologyllc.babyvax.template;

/**
 * Created by ${Arif} on 5/20/2016.
 */
public class Immunisation {

    private int immunisationID;
    private int pharmaID;
    private String immunisationName;
    private String immunisationDescription;

    public Immunisation(int immunisationID, int pharmaID, String immunisationName, String immunisationDescription) {
        this.immunisationID = immunisationID;
        this.pharmaID = pharmaID;
        this.immunisationName = immunisationName;
        this.immunisationDescription = immunisationDescription;
    }

    public Immunisation(int immunisationID, String immunisationName, String immunisationDescription) {
        this.immunisationID = immunisationID;
        this.immunisationName = immunisationName;
        this.immunisationDescription = immunisationDescription;
    }

    public Immunisation(String immunisationDescription, String immunisationName) {
        this.immunisationDescription = immunisationDescription;
        this.immunisationName = immunisationName;
    }



    public int getImmunisationID() {
        return immunisationID;
    }

    public void setImmunisationID(int immunisationID) {
        this.immunisationID = immunisationID;
    }

    public int getPharmaID() {
        return pharmaID;
    }

    public void setPharmaID(int pharmaID) {
        this.pharmaID = pharmaID;
    }

    public String getImmunisationName() {
        return immunisationName;
    }

    public void setImmunisationName(String immunisationName) {
        this.immunisationName = immunisationName;
    }

    public String getImmunisationDescription() {
        return immunisationDescription;
    }

    public void setImmunisationDescription(String immunisationDescription) {
        this.immunisationDescription = immunisationDescription;
    }
}
