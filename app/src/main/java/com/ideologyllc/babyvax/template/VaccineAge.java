package com.ideologyllc.babyvax.template;

/**
 * Created by ${Arif} on 5/19/2016.
 */
public class VaccineAge {

    private int id;
    private int immunisationID;
    private int vaccineAge;

    public VaccineAge(int id, int immunisationID, int vaccineAge) {
        this.id = id;
        this.immunisationID = immunisationID;
        this.vaccineAge = vaccineAge;
    }

    public VaccineAge( int immunisationID, int vaccineAge) {
        this.immunisationID = immunisationID;
        this.vaccineAge = vaccineAge;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImmunisationID() {
        return immunisationID;
    }

    public void setImmunisationID(int immunisationID) {
        this.immunisationID = immunisationID;
    }

    public int getVaccineAge() {
        return vaccineAge;
    }

    public void setVaccineAge(int vaccineAge) {
        this.vaccineAge = vaccineAge;
    }
}
