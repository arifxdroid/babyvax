package com.ideologyllc.babyvax.template;

import android.graphics.Color;

import com.ideologyllc.babyvax.R;
import java.util.ArrayList;

/**
 * Created by Arif on 5/8/2016.
 */
public class HomeGrid {
    int backGround;
    String name;

    public HomeGrid(int backGround, String name) {
        this.backGround = backGround;
        this.name = name;
    }

    public HomeGrid() {
    }

    public int getBackGround() {
        return backGround;
    }

    public void setBackGround(int backGround) {
        this.backGround = backGround;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<HomeGrid> homeInit(){

        ArrayList<HomeGrid> homeInit= new ArrayList<>();
        homeInit.add(new HomeGrid(R.color.color_new_pink, "Vaccination schedule"));
        homeInit.add(new HomeGrid(R.color.color_new_yellow, "About the vaccines"));
        homeInit.add(new HomeGrid(R.color.color_new_blue, "About the diseases"));
        homeInit.add(new HomeGrid(R.color.color_new_violet, "My family"));
        homeInit.add(new HomeGrid(R.color.color_new_green, "Vaccine history"));
        homeInit.add(new HomeGrid(R.color.color_new_yellow_dark, "Vaccine and autism"));
        homeInit.add(new HomeGrid(R.color.color_new_pink, "About the app"));
        homeInit.add(new HomeGrid(R.color.color_new_blue, "Information source"));
        return  homeInit;
    }
}
