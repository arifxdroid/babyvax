package com.ideologyllc.babyvax.template;

/**
 * Created by ${Arif} on 5/24/2016.
 */
public class Child {

    private int childID;
    private String childName;
    private String DOB;
    private String imageLink;

    public Child(int childID, String childName, String DOB, String imageLink) {
        this.childID = childID;
        this.childName = childName;
        this.DOB = DOB;
        this.imageLink = imageLink;
    }

    public Child(String childName, String DOB, String imageLink) {
        this.childName = childName;
        this.DOB = DOB;
        this.imageLink = imageLink;
    }

    public int getChildID() {
        return childID;
    }

    public void setChildID(int childID) {
        this.childID = childID;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
