package com.ideologyllc.babyvax.template;

import com.ideologyllc.babyvax.R;

import java.util.ArrayList;

/**
 * Created by arif on 8/30/15.
 */
public class DiseasesItem {

    int background;
    String name;

    public DiseasesItem(int background, String name) {
        this.background = background;
        this.name = name;
    }

    public DiseasesItem(){
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<DiseasesItem> init(){

        ArrayList<DiseasesItem> init = new ArrayList<>();

        init.add(new DiseasesItem(R.color.color_new_pink,"Diphtheria"));
        init.add(new DiseasesItem(R.color.color_new_yellow, "Haemophilus influenzae b (Hib)"));
        init.add(new DiseasesItem(R.color.color_new_blue, "Hepatitis B"));
        init.add(new DiseasesItem(R.color.color_new_violet, "Measles"));
        init.add(new DiseasesItem(R.color.color_new_green, "Meningococcal C (MenC)"));
        init.add(new DiseasesItem(R.color.color_new_yellow_dark, "Mumps"));
        init.add(new DiseasesItem(R.color.color_new_pink, "Pertussis (whooping cough)"));
        init.add(new DiseasesItem(R.color.color_new_blue, "Pneumococcal disease"));
        init.add(new DiseasesItem(R.color.color_new_yellow, "Polio"));
        init.add(new DiseasesItem(R.color.color_new_violet, "Rubella"));
        init.add(new DiseasesItem(R.color.color_new_green, "Tetanus"));
        init.add(new DiseasesItem(R.color.color_new_blue, "Tuberculosis (TB)"));

        return init;
    }

}
