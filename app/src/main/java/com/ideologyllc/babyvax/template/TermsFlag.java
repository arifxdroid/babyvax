package com.ideologyllc.babyvax.template;

/**
 * Created by ${Arif} on 7/1/2016.
 */
public class TermsFlag {

    int id;
    String status;

    public TermsFlag(int id, String status) {
        this.id = id;
        this.status = status;
    }

    public TermsFlag(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
