package com.ideologyllc.babyvax.template;

/**
 * Created by ${Arif} on 5/24/2016.
 */
public class ChildImmunisation {

    private int childImmunisationID;
    private int childID;
    private int vaccineAgeID;
    private String givenDate;

    public ChildImmunisation(int childImmunisationID, int childID, int vaccineAgeID, String givenDate) {
        this.childImmunisationID = childImmunisationID;
        this.childID = childID;
        this.vaccineAgeID = vaccineAgeID;
        this.givenDate = givenDate;
    }

    public ChildImmunisation(int childID, int vaccineAgeID, String givenDate) {
        this.childID = childID;
        this.vaccineAgeID = vaccineAgeID;
        this.givenDate = givenDate;
    }

    public int getChildImmunisationID() {
        return childImmunisationID;
    }

    public void setChildImmunisationID(int childImmunisationID) {
        this.childImmunisationID = childImmunisationID;
    }

    public int getChildID() {
        return childID;
    }

    public void setChildID(int childID) {
        this.childID = childID;
    }

    public int getVaccineAgeID() {
        return vaccineAgeID;
    }

    public void setVaccineAgeID(int vaccineAgeID) {
        this.vaccineAgeID = vaccineAgeID;
    }

    public String getGivenDate() {
        return givenDate;
    }

    public void setGivenDate(String givenDate) {
        this.givenDate = givenDate;
    }
}
