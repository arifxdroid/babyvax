package com.ideologyllc.babyvax.template;

/**
 * Created by ${Arif} on 5/20/2016.
 */
public class Pharma {

    private int pharmaID;
    private String pharmaName;
    private String pharmaDescription;
    private String pharmaCountry;

    public Pharma(int pharmaID, String pharmaName, String pharmaDescription, String pharmaCountry) {
        this.pharmaID = pharmaID;
        this.pharmaName = pharmaName;
        this.pharmaDescription = pharmaDescription;
        this.pharmaCountry = pharmaCountry;
    }

    public Pharma(String pharmaName, String pharmaDescription, String pharmaCountry) {
        this.pharmaName = pharmaName;
        this.pharmaDescription = pharmaDescription;
        this.pharmaCountry = pharmaCountry;
    }

    public int getPharmaID() {
        return pharmaID;
    }

    public void setPharmaID(int pharmaID) {
        this.pharmaID = pharmaID;
    }

    public String getPharmaName() {
        return pharmaName;
    }

    public void setPharmaName(String pharmaName) {
        this.pharmaName = pharmaName;
    }

    public String getPharmaDescription() {
        return pharmaDescription;
    }

    public void setPharmaDescription(String pharmaDescription) {
        this.pharmaDescription = pharmaDescription;
    }

    public String getPharmaCountry() {
        return pharmaCountry;
    }

    public void setPharmaCountry(String pharmaCountry) {
        this.pharmaCountry = pharmaCountry;
    }
}
