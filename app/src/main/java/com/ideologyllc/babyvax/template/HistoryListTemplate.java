package com.ideologyllc.babyvax.template;

/**
 * Created by ${Arif} on 5/31/2016.
 */
public class HistoryListTemplate {

    private String age;
    private String immunisation;
    private String givenDate;
    private int childImmunisationID;
    private int childID;
    private int vaccineAgeID;

    public HistoryListTemplate(String age, String immunisation, String givenDate) {
        this.age = age;
        this.immunisation = immunisation;
        this.givenDate = givenDate;
    }

    public HistoryListTemplate(String age, String immunisation, String givenDate, int childImmunisationID, int childID, int vaccineAgeID) {
        this.age = age;
        this.immunisation = immunisation;
        this.givenDate = givenDate;
        this.childImmunisationID = childImmunisationID;
        this.childID = childID;
        this.vaccineAgeID = vaccineAgeID;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getImmunisation() {
        return immunisation;
    }

    public void setImmunisation(String immunisation) {
        this.immunisation = immunisation;
    }

    public String getGivenDate() {
        return givenDate;
    }

    public void setGivenDate(String givenDate) {
        this.givenDate = givenDate;
    }

    public int getChildImmunisationID() {
        return childImmunisationID;
    }

    public void setChildImmunisationID(int childImmunisationID) {
        this.childImmunisationID = childImmunisationID;
    }

    public int getChildID() {
        return childID;
    }

    public void setChildID(int childID) {
        this.childID = childID;
    }

    public int getVaccineAgeID() {
        return vaccineAgeID;
    }

    public void setVaccineAgeID(int vaccineAgeID) {
        this.vaccineAgeID = vaccineAgeID;
    }
}
