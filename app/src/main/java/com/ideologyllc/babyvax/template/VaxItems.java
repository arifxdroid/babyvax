package com.ideologyllc.babyvax.template;

/**
 * Created by arif on 9/1/15.
 */
public class VaxItems {

    String name;
    String period;
    String about;

    public VaxItems(String name, String period, String about) {
        this.name = name;
        this.period = period;
        this.about = about;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
