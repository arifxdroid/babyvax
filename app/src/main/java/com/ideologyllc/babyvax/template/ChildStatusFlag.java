package com.ideologyllc.babyvax.template;

/**
 * Created by ${Arif} on 5/30/2016.
 */
public class ChildStatusFlag {

    int id;
    int childId;
    String status;

    public ChildStatusFlag(int id, int childId, String status) {
        this.id = id;
        this.childId = childId;
        this.status = status;
    }

    public ChildStatusFlag(int childId, String status) {
        this.childId = childId;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChildId() {
        return childId;
    }

    public void setChildId(int childId) {
        this.childId = childId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
