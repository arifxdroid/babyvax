package com.ideologyllc.babyvax.template;

/**
 * Created by ${Arif} on 6/30/2016.
 */
public class ImmunisationPrivate {

    private int id;
    private int childID;
    private String name;
    private String description;
    private String pharmaName;
    private String givenDate;
    private String doctorName;

    public ImmunisationPrivate(int id, int childID, String name, String description, String pharmaName, String givenDate, String doctorName) {
        this.id = id;
        this.childID = childID;
        this.name = name;
        this.description = description;
        this.pharmaName = pharmaName;
        this.givenDate = givenDate;
        this.doctorName = doctorName;
    }

    public ImmunisationPrivate(int childID, String name, String description, String pharmaName, String givenDate, String doctorName) {
        this.childID = childID;
        this.name = name;
        this.description = description;
        this.pharmaName = pharmaName;
        this.givenDate = givenDate;
        this.doctorName = doctorName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChildID() {
        return childID;
    }

    public void setChildID(int childID) {
        this.childID = childID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPharmaName() {
        return pharmaName;
    }

    public void setPharmaName(String pharmaName) {
        this.pharmaName = pharmaName;
    }

    public String getGivenDate() {
        return givenDate;
    }

    public void setGivenDate(String givenDate) {
        this.givenDate = givenDate;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }
}
