package com.ideologyllc.babyvax.template;

/**
 * Created by arif on 8/15/15.
 */
public class NonHse {

    private int id_NonHse;
    private String age;
    private String immunisation;
    private String manufacturer;

    public NonHse(int id_NonHse, String age, String immunisation, String manufacturer) {
        this.id_NonHse = id_NonHse;
        this.age = age;
        this.immunisation = immunisation;
        this.manufacturer = manufacturer;
    }

    public NonHse(String age, String immunisation, String manufacturer) {
        this.age = age;
        this.immunisation = immunisation;
        this.manufacturer = manufacturer;
    }

    public int getId_NonHse() {
        return id_NonHse;
    }

    public void setId_NonHse(int id_NonHse) {
        this.id_NonHse = id_NonHse;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getImmunisation() {
        return immunisation;
    }

    public void setImmunisation(String immunisation) {
        this.immunisation = immunisation;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}
