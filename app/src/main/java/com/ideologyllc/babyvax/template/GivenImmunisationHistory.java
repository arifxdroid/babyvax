package com.ideologyllc.babyvax.template;

/**
 * Created by ${Arif} on 6/12/2016.
 */
public class GivenImmunisationHistory {

    private int id;
    private int childImmunisationID;
    private int pharmaID;
    private String hseNumber;
    private String doctorName;
    private String lot;

    public GivenImmunisationHistory(int id, int childImmunisationID, int pharmaID, String hseNumber, String doctorName, String lot) {
        this.id = id;
        this.childImmunisationID = childImmunisationID;
        this.pharmaID = pharmaID;
        this.hseNumber = hseNumber;
        this.doctorName = doctorName;
        this.lot = lot;
    }

    public GivenImmunisationHistory(int childImmunisationID, int pharmaID, String hseNumber, String doctorName, String lot) {
        this.childImmunisationID = childImmunisationID;
        this.pharmaID = pharmaID;
        this.hseNumber = hseNumber;
        this.doctorName = doctorName;
        this.lot = lot;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChildImmunisationID() {
        return childImmunisationID;
    }

    public void setChildImmunisationID(int childImmunisationID) {
        this.childImmunisationID = childImmunisationID;
    }

    public int getPharmaID() {
        return pharmaID;
    }

    public void setPharmaID(int pharmaID) {
        this.pharmaID = pharmaID;
    }

    public String getHseNumber() {
        return hseNumber;
    }

    public void setHseNumber(String hseNumber) {
        this.hseNumber = hseNumber;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }
}
