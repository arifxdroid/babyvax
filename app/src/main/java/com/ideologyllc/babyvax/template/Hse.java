package com.ideologyllc.babyvax.template;

/**
 * Created by arif on 8/15/15.
 */
public class Hse {

    private int id_hse;
    private String age;
    private String immunisation;
    private String manufacturer;

    public Hse(int id_hse, String age, String immunisation, String manufacturer) {
        this.id_hse = id_hse;
        this.age = age;
        this.immunisation = immunisation;
        this.manufacturer = manufacturer;
    }

    public Hse(String age, String immunisation, String manufacturer) {
        this.age = age;
        this.immunisation = immunisation;
        this.manufacturer = manufacturer;
    }

    public int getId_hse() {
        return id_hse;
    }

    public void setId_hse(int id_hse) {
        this.id_hse = id_hse;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getImmunisation() {
        return immunisation;
    }

    public void setImmunisation(String immunisation) {
        this.immunisation = immunisation;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }
}
