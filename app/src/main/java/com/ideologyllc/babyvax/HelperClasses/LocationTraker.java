package com.ideologyllc.babyvax.HelperClasses;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LocationTraker implements LocationListener {
    private Context context;
    private LocationManager locationManager;
    private double latitude, longitude;
    private boolean locationState = false;
    private boolean gpsEnabled = false;
    private boolean NetworkEnabled = false;
    private boolean PassiveEnabled = false;
    private Location location;
    private boolean isGpsEnable = false;

    public LocationTraker(Context context) {
        this.context = context;
        getLocation();
    }

    private Location getLocation() {

        locationManager = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);

        gpsEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        NetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        PassiveEnabled = locationManager
                .isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

        if (NetworkEnabled) {
            try {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            } catch (SecurityException e) {
//                Toast.makeText(context, "Error3 !!", Toast.LENGTH_LONG).show();
            }

            if (locationManager != null) {
                locationState = true;
                try {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                } catch (SecurityException e) {
//                    Toast.makeText(context, "Error4 !!", Toast.LENGTH_LONG).show();
                }
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                }

            }

        } else if (gpsEnabled) {

            try {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            } catch (SecurityException e) {
//                Toast.makeText(context, "Error1 !!", Toast.LENGTH_LONG).show();
            }

            if (locationManager != null) {
                locationState = true;
                try {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                } catch (SecurityException e) {
//                    Toast.makeText(context, "Error2 !!", Toast.LENGTH_LONG).show();
                }

                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                }

            }

        } else if (PassiveEnabled) {
            try {
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, this);
            } catch (SecurityException e) {
//                Toast.makeText(context, "Error5 !!", Toast.LENGTH_LONG).show();
            }

            if (locationManager != null) {
                locationState = true;
                try {
                    location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                } catch (SecurityException e) {
//                    Toast.makeText(context, "Error6 !!", Toast.LENGTH_LONG).show();
                }
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();

                }

            }

        }

        return location;
    }

    public double getlat() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        return latitude;
    }

    public double getLon() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        return longitude;
    }

    public boolean getState() {

        return locationState;
    }

    public boolean isGpsEnable(){
        if (gpsEnabled){
            this.isGpsEnable = true;
        }
        return this.isGpsEnable;
    }

    public void showSettingsAlert(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("GPS is settings");
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }
    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    public String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        Geocoder geocoder;
        String address = null;
        String city = null;
        String country = null;
        String postalCode = null;
        String knownName = null;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getAddressLine(1);
            country = addresses.get(0).getAddressLine(2);

            postalCode = addresses.get(0).getAddressLine(3);
            knownName = addresses.get(0).getAdminArea();

            Log.i("TAG", "----" + address);
            Log.i("TAG", "---" + city);
            Log.i("TAG", "---" + country);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            Log.i("TAG", "----" + e.toString());
        }

//        return address + ",\n" + city + ",\n" + country;
        return "" + address + "," + city + "," + knownName + "," + country;
    }

    public String getCountryName(double LATITUDE, double LONGITUDE) {
        Geocoder geocoder;
        String address = null;
        String city = null;
        String country = null;
        String postalCode = null;
        String knownName = null;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            address = addresses.get(0).getAddressLine(0);
            city = addresses.get(0).getAddressLine(1);
            country = addresses.get(0).getAddressLine(2);

            postalCode = addresses.get(0).getAddressLine(3);
            knownName = addresses.get(0).getAdminArea();

            Log.i("TAG", "----" + address);
            Log.i("TAG", "---" + city);
            Log.i("TAG", "---" + country);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            Log.i("TAG", "----" + e.toString());
        }

//        return address + ",\n" + city + ",\n" + country;
        return "" + country;
    }
}
