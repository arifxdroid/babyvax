package com.ideologyllc.babyvax.HelperClasses;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

/**
 * Created by ${Arif} on 8/1/2016.
 */

public class SessionHelper {

    private static String TAG = SessionHelper.class.getSimpleName();
    SharedPreferences pref;

    Editor editor;
    Context _context;

    private static final String PREF_NAME = "BabyVaxLogin";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

    public SessionHelper(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.commit();
        Log.d(TAG, "User login session modified!");
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }
}
