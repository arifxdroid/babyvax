package com.ideologyllc.babyvax.HelperClasses;

import java.util.Calendar;

/**
 * Created by ${Arif} on 5/24/2016.
 */
public class AgeCalculation {

    private String DOB;

    private int startYear;
    private int startMonth;
    private int startDay;
    private int endYear;
    private int endMonth;
    private int endDay;


    /**
     *
     * @param DOB format "yyyy-MM-dd"
     */
    public AgeCalculation(String DOB) {

        this.DOB = DOB;
        this.startYear = getYear();
        this.startMonth = getMonth();
        this.startDay = getDay();

        Calendar end=Calendar.getInstance();
        endYear=end.get(Calendar.YEAR);
        endMonth=end.get(Calendar.MONTH);
        endDay=end.get(Calendar.DAY_OF_MONTH);
    }

    public String getAge(){

        int resYear;
        int resMonth;
        int resDay;

        resYear = endYear-startYear;

        if(endMonth>=startMonth)
        {
            resMonth= endMonth-startMonth;
        }
        else
        {
            resMonth=endMonth-startMonth;
            resMonth=12+resMonth;
            resYear--;
        }

        if(endDay>=startDay)
        {
            resDay= endDay-startDay;
        }
        else
        {
            resDay=endDay-startDay;
            resDay=30+resDay;
            if(resMonth==0)
            {
                resMonth=11;
                resYear--;
            }
            else
            {
                resMonth--;
            }

        }
        return resYear+" Year, "+resMonth+" Month, "+resDay+" Day";
    }

    public int getYear(){

        int count = 0;
        int year;
        String temp = "";
        for (int i = 0; i < DOB.length(); i++) {

            if (DOB.charAt(i) == '-') {
                count = 1;
                break;
            }

            if(count == 0){

                temp += DOB.charAt(i);
            }
        }
        String myVar = temp.replace("-", "");
        year = Integer.parseInt(myVar);
        return year;
    }

    public int getMonth(){

        int count = 0;
        int month;
        String temp = "";
        for (int i = 0; i < DOB.length(); i++) {

            if (DOB.charAt(i) == '-') {
                count += 1;
            }

            if(count == 1){

                temp += DOB.charAt(i);
            }
        }
        String myVar = temp.replace("-", "");
        month = Integer.parseInt(myVar);
        return month;
    }

    public int getDay(){

        int count = 0;
        int day;
        String temp = "";
        for (int i = 0; i < DOB.length(); i++) {

            if (DOB.charAt(i) == '-') {
                count += 1;
            }

            if(count == 2){

                temp += DOB.charAt(i);
            }
        }
        String myVar = temp.replace("-", "");
        day = Integer.parseInt(myVar);
        return day;
    }
}
