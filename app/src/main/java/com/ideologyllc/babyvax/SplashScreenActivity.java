package com.ideologyllc.babyvax;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.ideologyllc.babyvax.HelperClasses.NetworkUtil;
import com.ideologyllc.babyvax.database.FlagHelperDB;

public class SplashScreenActivity extends AppCompatActivity {

    private static int splash_time_out = 1000;
    private NetworkUtil networkUtil;
    FlagHelperDB flagHelperDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        networkUtil = new NetworkUtil(getApplicationContext());
        flagHelperDB = new FlagHelperDB(getApplicationContext());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (networkUtil.isConnectingToInternet(getApplicationContext())){

                    if (flagHelperDB.checkLocation() == null){

                        Intent i = new Intent(SplashScreenActivity.this, MapsActivity.class);
                        startActivity(i);
                        finish();
                    }else {

                        Intent i = new Intent(SplashScreenActivity.this, MainHome.class);
                        startActivity(i);
                        finish();
                    }

                }else {

                    Intent i = new Intent(SplashScreenActivity.this, MainHome.class);
                    startActivity(i);
                    finish();
                }
            }
        }, splash_time_out);

    }


}
