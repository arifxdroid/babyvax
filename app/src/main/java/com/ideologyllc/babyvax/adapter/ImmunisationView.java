package com.ideologyllc.babyvax.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.template.Hse;
import com.ideologyllc.babyvax.template.VaxItems;

import java.util.List;

/**
 * Created by arif on 8/22/15.
 */
public class ImmunisationView extends RecyclerView.Adapter<ImmunisationView.ViewHolder> {

    private LayoutInflater inflater;
    private List<VaxItems> vaxItemses;
    OnItemClickListener onItemClickListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView txtImmunisationName;
        TextView txtAge, txtAbout;
        CardView card_view;

        public ViewHolder(View itemView) {

            super(itemView);
            txtImmunisationName = (TextView) itemView.findViewById(R.id.txtImmunisationName);
            txtAge = (TextView)itemView.findViewById(R.id.txtAge);
            txtAbout = (TextView)itemView.findViewById(R.id.txtAbout);
            card_view = (CardView)itemView.findViewById(R.id.card_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null){

                onItemClickListener.onItemClick(view, getPosition());
            }
        }
    }

    public interface OnItemClickListener{

        public void onItemClick(View view, int position);
    }


    public void SetOnItemClickListener(final OnItemClickListener onItemClickListener){

        this.onItemClickListener = onItemClickListener;
    }



    public ImmunisationView(Context context, List<VaxItems> vaxItemses){

        inflater = LayoutInflater.from(context);
        this.vaxItemses = vaxItemses;

    }

    public  ImmunisationView(List<VaxItems> vaxItemses){

        this.vaxItemses = vaxItemses;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_immunisation_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        VaxItems vaxItems = vaxItemses.get(position);
        holder.txtAge.setText(vaxItems.getPeriod());
        holder.txtImmunisationName.setText(vaxItems.getName());
        holder.txtAbout.setText(vaxItems.getAbout());
    }



    @Override
    public int getItemCount() {
        return vaxItemses.size();
    }
}
