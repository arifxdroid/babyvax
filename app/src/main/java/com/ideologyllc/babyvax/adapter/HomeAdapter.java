package com.ideologyllc.babyvax.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.template.HomeGrid;

import java.util.ArrayList;

/**
 * Created by Arif on 10/10/2015.
 */
public class HomeAdapter extends ArrayAdapter<HomeGrid> {
    Context context;
    int layoutID;
    ArrayList<HomeGrid> item = new ArrayList<>();

    public HomeAdapter(Context context, int resource, ArrayList<HomeGrid> item) {
        super(context, resource, item);
        this.context = context;
        this.layoutID = resource;
        this.item = item;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        HomeHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutID, parent, false);
            holder = new HomeHolder();
            holder.txtHome = (TextView) row.findViewById(R.id.txtHome);
            holder.imageViewHome = (ImageView) row.findViewById(R.id.imageViewHome);
            holder.card_view = (CardView)row.findViewById(R.id.card_view);
            row.setTag(holder);
        } else {
            holder = (HomeHolder) row.getTag();
        }
        HomeGrid homeGrid = item.get(position);
        holder.txtHome.setText(homeGrid.getName());
        holder.imageViewHome.setImageResource(homeGrid.getBackGround());
        return row;
    }

    static  class HomeHolder{
        ImageView imageViewHome;
        TextView txtHome;
        CardView card_view;
    }
}
