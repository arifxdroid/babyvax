package com.ideologyllc.babyvax.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.template.HistoryListTemplate;

import java.util.ArrayList;

/**
 * Created by ${Arif} on 5/31/2016.
 */
public class ChildHistoryView extends RecyclerView.Adapter<ChildHistoryView.ViewHolder> {

    OnItemClickListener onItemClickListener;
    ArrayList<HistoryListTemplate> listTemplates;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_child_history_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChildHistoryView.ViewHolder holder, int position) {

        HistoryListTemplate listElement = listTemplates.get(position);
        holder.textViewAge.setText(listElement.getAge());
        holder.textViewImmunisation.setText(listElement.getImmunisation());
        holder.textViewGivenDate.setText(listElement.getGivenDate());

    }

    @Override
    public int getItemCount() {
        return listTemplates.size();
    }

    public ChildHistoryView(ArrayList<HistoryListTemplate> listTemplates){
        this.listTemplates = listTemplates;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView textViewAge, textViewImmunisation, textViewGivenDate;
        public ViewHolder(View itemView) {
            super(itemView);
            textViewAge = (TextView) itemView.findViewById(R.id.textViewAge);
            textViewImmunisation = (TextView) itemView.findViewById(R.id.textViewImmunisation);
            textViewGivenDate = (TextView) itemView.findViewById(R.id.textViewGivenDate);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickListener != null){
                onItemClickListener.onItemClick(itemView, getPosition());
            }
        }
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
