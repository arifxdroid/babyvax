package com.ideologyllc.babyvax.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.template.DiseasesItem;
import java.util.List;

/**
 * Created by ${Arif} on 5/8/2016.
 */
public class DiseasesView extends RecyclerView.Adapter<DiseasesView.ViewHolder> {

    private LayoutInflater inflater;
    OnItemClickListener onItemClickListener;
    List<DiseasesItem> item;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView imageViewHome;
        TextView txtHome;
        CardView card_view;

        public ViewHolder(View itemView) {

            super(itemView);
            txtHome = (TextView) itemView.findViewById(R.id.txtHome);
            imageViewHome = (ImageView) itemView.findViewById(R.id.imageViewHome);
            card_view = (CardView)itemView.findViewById(R.id.card_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null){

                onItemClickListener.onItemClick(view, getPosition());
            }
        }
    }

    public interface OnItemClickListener{

        public void onItemClick(View view, int position);
    }


    public void SetOnItemClickListener(final OnItemClickListener onItemClickListener){

        this.onItemClickListener = onItemClickListener;
    }



    public DiseasesView(Context context, List<DiseasesItem> item){

        inflater = LayoutInflater.from(context);
        this.item = item;

    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_home_grid, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        DiseasesItem diseasesItem = item.get(position);
        holder.txtHome.setText(diseasesItem.getName());
        holder.imageViewHome.setImageResource(diseasesItem.getBackground());
    }



    @Override
    public int getItemCount() {
        return item.size();
    }
}
