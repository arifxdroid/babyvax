package com.ideologyllc.babyvax.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.template.HomeGrid;
import com.ideologyllc.babyvax.template.Hse;
import com.ideologyllc.babyvax.template.VaxItems;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arif on 4/26/16.
 */
public class HomeView extends RecyclerView.Adapter<HomeView.ViewHolder> {

    private LayoutInflater inflater;
    OnItemClickListener onItemClickListener;
    List<HomeGrid> item;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView imageViewHome;
        TextView txtHome;
        CardView card_view;

        public ViewHolder(View itemView) {

            super(itemView);
            txtHome = (TextView) itemView.findViewById(R.id.txtHome);
            imageViewHome = (ImageView) itemView.findViewById(R.id.imageViewHome);
            card_view = (CardView)itemView.findViewById(R.id.card_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null){

                onItemClickListener.onItemClick(view, getPosition());
            }
        }
    }

    public interface OnItemClickListener{

        public void onItemClick(View view, int position);
    }


    public void SetOnItemClickListener(final OnItemClickListener onItemClickListener){

        this.onItemClickListener = onItemClickListener;
    }



    public HomeView(Context context, List<HomeGrid> item){

        inflater = LayoutInflater.from(context);
        this.item = item;

    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_home_grid, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        HomeGrid homeGrid = item.get(position);
        holder.txtHome.setText(homeGrid.getName());
        holder.imageViewHome.setImageResource(homeGrid.getBackGround());
    }



    @Override
    public int getItemCount() {
        return item.size();
    }
}
