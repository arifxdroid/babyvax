package com.ideologyllc.babyvax.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.template.NonHse;

import java.util.List;

/**
 * Created by arif on 8/18/15.
 */
public class NonHseViewAdapter extends ArrayAdapter<NonHse> {

    public NonHseViewAdapter(Context context, List<NonHse> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null){

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_hse_view, null);

            holder = new ViewHolder();

            holder.txtViewAge = (TextView)convertView.findViewById(R.id.txtViewAge);
            holder.txtViewImmunisation = (TextView)convertView.findViewById(R.id.txtViewImmunisation);
            holder.txtViewManufacturer = (TextView)convertView.findViewById(R.id.txtViewManufacturer);

            convertView.setTag(holder);

        }else {

            holder = (ViewHolder) convertView.getTag();
        }

        NonHse nonHse = getItem(position);
        holder.txtViewAge.setText(nonHse.getAge());
        holder.txtViewImmunisation.setText(nonHse.getImmunisation());
        holder.txtViewManufacturer.setText(nonHse.getManufacturer());
        return convertView;
    }


    static class ViewHolder{

        public TextView txtViewAge;
        public TextView txtViewImmunisation;
        public TextView txtViewManufacturer;

    }
}
