package com.ideologyllc.babyvax.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ideologyllc.babyvax.HelperClasses.AgeCalculation;
import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.template.Child;
import com.ideologyllc.babyvax.template.ImmunisationPrivate;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

/**
 * Created by ${Arif} on 6/30/2016.
 */
public class AdditionalView extends RecyclerView.Adapter<AdditionalView.ViewHolder> {

    Context context;
    ArrayList<ImmunisationPrivate> immunisationPrivates;
    OnItemClickListener onItemClickListener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_additional_immunisation_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ImmunisationPrivate immunisationPrivate = immunisationPrivates.get(position);
        holder.textViewImmunisationName.setText(immunisationPrivate.getName());
        holder.textViewDate.setText(immunisationPrivate.getGivenDate());

    }

    @Override
    public int getItemCount() {
        return immunisationPrivates.size();
    }

    public AdditionalView(Context context, ArrayList<ImmunisationPrivate> immunisationPrivates) {
        this.context = context;
        this.immunisationPrivates = immunisationPrivates;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView textViewImmunisationName;
        TextView textViewDate;
        CardView cardView;


        public ViewHolder(View itemView) {
            super(itemView);
            textViewImmunisationName = (TextView)itemView.findViewById(R.id.textViewImmunisationName);
            textViewDate = (TextView)itemView.findViewById(R.id.textViewDate);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (onItemClickListener != null){
                onItemClickListener.onItemClick(itemView, getPosition());
            }
        }
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }


    public void SetOnItemClickListener(final OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }
}
