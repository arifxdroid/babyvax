package com.ideologyllc.babyvax.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.template.Hse;

import java.util.List;

/**
 * Created by arif on 8/16/15.
 */
public class HseViewAdapter extends ArrayAdapter<Hse> {

    public HseViewAdapter(Context context, List<Hse> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView == null){

            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_hse_view, null);

            holder = new ViewHolder();

            holder.txtViewAge = (TextView)convertView.findViewById(R.id.txtViewAge);
            holder.txtViewImmunisation = (TextView)convertView.findViewById(R.id.txtViewImmunisation);
            holder.txtViewManufacturer = (TextView)convertView.findViewById(R.id.txtViewManufacturer);

            convertView.setTag(holder);

        }else {

            holder = (ViewHolder) convertView.getTag();
        }

        Hse hse = getItem(position);
        holder.txtViewAge.setText(hse.getAge());
        holder.txtViewImmunisation.setText(hse.getImmunisation());
        holder.txtViewManufacturer.setText(hse.getManufacturer());
        return convertView;
    }


    static class ViewHolder{

        public TextView txtViewAge;
        public TextView txtViewImmunisation;
        public TextView txtViewManufacturer;

    }
}
