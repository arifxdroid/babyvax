package com.ideologyllc.babyvax.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ideologyllc.babyvax.HelperClasses.AgeCalculation;
import com.ideologyllc.babyvax.R;
import com.ideologyllc.babyvax.template.Child;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

/**
 * Created by ${Arif} on 5/25/2016.
 */
public class ChildView extends RecyclerView.Adapter<ChildView.ViewHolder> {

    Context context;
    ArrayList<Child> children;
    OnItemClickListener onItemClickListener;
    OnLongClickListener onLongClickListener;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_children_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Child child = children.get(position);
        holder.imageChild.setImageURI(Uri.parse(child.getImageLink()));
        holder.textName.setText(child.getChildName());
        holder.textAge.setText(new AgeCalculation(child.getDOB()).getAge());

    }

    @Override
    public int getItemCount() {
        return children.size();
    }

    public ChildView(Context context, ArrayList<Child> children) {
        this.context = context;
        this.children = children;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener, View.OnLongClickListener{

        CircularImageView imageChild;
        TextView textName;
        TextView textDue;
        TextView textAge;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageChild = (CircularImageView ) itemView.findViewById(R.id.imageChild);
            textName = (TextView)itemView.findViewById(R.id.textName);
            textDue = (TextView)itemView.findViewById(R.id.textDue);
            textAge = (TextView)itemView.findViewById(R.id.textAge);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
            itemView.setOnClickListener(this);
            //itemView.setOnLongClickListener(null);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {

            if (onItemClickListener != null){
                onItemClickListener.onItemClick(itemView, getPosition());
            }
        }

        @Override
        public boolean onLongClick(View v) {

            if (onLongClickListener != null){
                onLongClickListener.onLongPress(itemView, getPosition());
            }
            return true;
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(Menu.NONE, R.id.action_delete,
                    Menu.NONE, "Delete");

        }


        @Override
        public boolean onMenuItemClick(MenuItem item) {
            int position = getPosition();
            Child child = children.get(position);
            int id = item.getItemId();
            if (id == R.id.action_delete) {
                Toast.makeText(context, child.getChildName(), Toast.LENGTH_LONG).show();
            }
            return false;
        }
    }

    public interface OnItemClickListener{
        void onItemClick(View view, int position);
    }

    public interface OnLongClickListener{
        void onLongPress(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }

    public void SetOnLongClickListener(final OnLongClickListener onLongClickListener){
        this.onLongClickListener = onLongClickListener;
    }


}
