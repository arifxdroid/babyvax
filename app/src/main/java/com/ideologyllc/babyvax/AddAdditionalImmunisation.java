package com.ideologyllc.babyvax;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ideologyllc.babyvax.HelperClasses.AgeCalculation;
import com.ideologyllc.babyvax.database.ChildrenDb;
import com.ideologyllc.babyvax.template.ImmunisationPrivate;

import java.util.Calendar;

public class AddAdditionalImmunisation extends AppCompatActivity {

    private String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    EditText input_immunisationName, input_immunisationDescription, input_immunisationPharma, input_immunisationDoctor;
    TextView textViewDate;
    ImageView imageViewDatePicker;
    Button buttonSave;
    private Intent intent;
    private int childId;
    private int immunisationPrivateID;
    private ChildrenDb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_additional_immunisation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = new ChildrenDb(this);
        initialize();
        if (intent != null){
            Bundle b = intent.getExtras();
            childId = b.getInt("id", -1);
            immunisationPrivateID = b.getInt("immunisationPrivateID", -1);
        }

        if (immunisationPrivateID > 0){
            input_immunisationName.setText(db.getAImmunisationPrivate(immunisationPrivateID).getName());
            input_immunisationDescription.setText(db.getAImmunisationPrivate(immunisationPrivateID).getDescription());
            input_immunisationPharma.setText(db.getAImmunisationPrivate(immunisationPrivateID).getPharmaName());
            input_immunisationDoctor.setText(db.getAImmunisationPrivate(immunisationPrivateID).getDoctorName());
            textViewDate.setText(db.getAImmunisationPrivate(immunisationPrivateID).getGivenDate());
        }

        imageViewDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(1);
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if (immunisationPrivateID > 0){

                   if (input_immunisationName.getText().toString().length() < 2){
                       Snackbar.make(v, "Name can't be empty", Snackbar.LENGTH_LONG)
                               .setAction("Action", null).show();
                   }else {

                       String name = input_immunisationName.getText().toString();
                       String description = input_immunisationDescription.getText().toString();
                       String pharmaName = input_immunisationPharma.getText().toString();
                       String doctor = input_immunisationDoctor.getText().toString();
                       String givenDate;
                       if (textViewDate.getText().toString() != null){
                           givenDate = textViewDate.getText().toString();
                       }else givenDate = "Due";
                       ImmunisationPrivate immunisationPrivate = new ImmunisationPrivate(immunisationPrivateID, childId, name, description, pharmaName, givenDate, doctor);
                       long updated = db.updateImmunisationPrivate(immunisationPrivate);
                       if (updated > 0){
                           Intent i = new Intent(getApplicationContext(), ChildImmunisationHistory.class);
                           i.putExtra("id", childId);
                           startActivity(i);
                           finish();
                       }
                   }

               }else {

                   if (input_immunisationName.getText().toString().length() < 2){
                       Snackbar.make(v, "Name can't be empty", Snackbar.LENGTH_LONG)
                               .setAction("Action", null).show();
                   }else {

                       String name = input_immunisationName.getText().toString();
                       String description = input_immunisationDescription.getText().toString();
                       String pharmaName = input_immunisationPharma.getText().toString();
                       String doctor = input_immunisationDoctor.getText().toString();
                       String givenDate;
                       if (textViewDate.getText().toString() != null){
                           givenDate = textViewDate.getText().toString();
                       }else givenDate = "Due";
                       ImmunisationPrivate immunisationPrivate = new ImmunisationPrivate(childId, name, description, pharmaName, givenDate, doctor);
                       long added = db.addImmunisationPrivate(immunisationPrivate);
                       if (added > 0){
                           Intent i = new Intent(getApplicationContext(), ChildImmunisationHistory.class);
                           i.putExtra("id", childId);
                           startActivity(i);
                           finish();
                       }
                   }
               }
            }
        });
    }

    public void initialize(){

        input_immunisationName = (EditText) findViewById(R.id.input_immunisationName);
        input_immunisationDescription = (EditText) findViewById(R.id.input_immunisationDescription);
        input_immunisationPharma = (EditText) findViewById(R.id.input_immunisationPharma);
        input_immunisationDoctor = (EditText) findViewById(R.id.input_immunisationDoctor);
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        imageViewDatePicker = (ImageView) findViewById(R.id.imageViewDatePicker);
        buttonSave = (Button) findViewById(R.id.buttonSave);
        intent = getIntent();
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        switch (id){

            case 1:

                return new DatePickerDialog(this,datePickerListener, year, month, day);

        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

            if (textViewDate.getText().toString() != null){
                textViewDate.setText(" ");
                textViewDate.setText(dayOfMonth+"-"+months[monthOfYear]+"-"+year);
            }else {
                textViewDate.setText(dayOfMonth+"-"+months[monthOfYear]+"-"+year);
            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(getApplicationContext(), ChildImmunisationHistory.class);
        i.putExtra("id", childId);
        startActivity(i);
        finish();
    }
}
