package com.ideologyllc.babyvax;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ideologyllc.babyvax.HelperClasses.AgeCalculation;
import com.ideologyllc.babyvax.database.ChildrenDb;
import com.ideologyllc.babyvax.template.Child;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.Calendar;

public class AddChildren extends AppCompatActivity {

    private String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

    ImageView imageViewDatePicker;
    CircularImageView imageViewPick;
    TextView textViewShowDate, textAgeView;
    EditText input_name;
    Button buttonSave;
    ChildrenDb db;
    String DOB;
    String ImageLink;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_children);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initialize();
        db = new ChildrenDb(this);
        imageViewDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(1);
            }
        });

        imageViewPick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(i, "Choose from Gallery"), 1);
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                name = input_name.getText().toString();
                if (input_name.getText().length() < 2 ){

                    Snackbar.make(v, "Name and Date of birth can't be empty", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }else if (DOB == null){

                    Snackbar.make(v, "Name and Date of birth can't be empty", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }else {

                    Child child = new Child(input_name.getText().toString(), DOB, ImageLink);
                    long added = db.addChild(child);

                    if (added < 0){
                        Snackbar.make(v, "Error: Problem with adding Children profile", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }else {

                        Intent i = new Intent(AddChildren.this, MainActivity.class);
                        i.putExtra("name", "My family");
                        startActivity(i);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                    }
                }
            }
        });

    }

    public void initialize(){

        imageViewDatePicker = (ImageView)findViewById(R.id.imageViewDatePicker);
        imageViewPick = (CircularImageView)findViewById(R.id.imageViewPick);
        textViewShowDate = (TextView)findViewById(R.id.textViewShowDate);
        textAgeView = (TextView)findViewById(R.id.textAgeView);
        input_name = (EditText)findViewById(R.id.input_name);
        buttonSave = (Button)findViewById(R.id.buttonSave);
        ImageLink = "android.resource://"+getPackageName()+"/"+R.drawable.photoframe;
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        switch (id){

            case 1:

                return new DatePickerDialog(this,datePickerListener, year, month, day);

        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

            if (textViewShowDate.getText().toString() != null){

                textViewShowDate.setText(" ");
                textViewShowDate.setText(dayOfMonth+"-"+months[monthOfYear]+"-"+year);
                DOB = year+"-"+monthOfYear+"-"+dayOfMonth;
                textAgeView.setText(new AgeCalculation(DOB).getAge());

            }else {

                textViewShowDate.setText(dayOfMonth+"-"+months[monthOfYear]+"-"+year);
                DOB = year+"-"+monthOfYear+"-"+dayOfMonth;
            }

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == 1 && data != null){
//            imageViewPick.setImageURI(data.getData());
            ImageLink = data.getData().toString();
            imageViewPick.setImageURI(Uri.parse(ImageLink));
        }
        else {
            Log.d("Request code", String.valueOf(requestCode));
            imageViewPick.setImageURI(Uri.parse(ImageLink));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(AddChildren.this, MainActivity.class);
        i.putExtra("name", "My family");
        startActivity(i);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
    }


}
