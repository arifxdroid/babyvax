package com.ideologyllc.babyvax;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.FragmentTransaction;

import com.ideologyllc.babyvax.diseaseFragments.Bcg;
import com.ideologyllc.babyvax.diseaseFragments.Diphtheria;
import com.ideologyllc.babyvax.diseaseFragments.Haemophilus;
import com.ideologyllc.babyvax.diseaseFragments.HepatitisB;
import com.ideologyllc.babyvax.diseaseFragments.Hib;
import com.ideologyllc.babyvax.diseaseFragments.Measles;
import com.ideologyllc.babyvax.diseaseFragments.Menc;
import com.ideologyllc.babyvax.diseaseFragments.Meningococcal;
import com.ideologyllc.babyvax.diseaseFragments.Mmr;
import com.ideologyllc.babyvax.diseaseFragments.Mumps;
import com.ideologyllc.babyvax.diseaseFragments.Pcv;
import com.ideologyllc.babyvax.diseaseFragments.Pertussis;
import com.ideologyllc.babyvax.diseaseFragments.Pneumococcal;
import com.ideologyllc.babyvax.diseaseFragments.Polio;
import com.ideologyllc.babyvax.diseaseFragments.Rubella;
import com.ideologyllc.babyvax.diseaseFragments.SixInOne;
import com.ideologyllc.babyvax.diseaseFragments.Tetanus;
import com.ideologyllc.babyvax.diseaseFragments.Tuberculosis;

public class ImmunisationDetails extends AppCompatActivity {

    Diphtheria diphtheria;
    Haemophilus haemophilus;
    HepatitisB hepatitisB;
    Measles measles;
    Meningococcal meningococcal;
    Mumps mumps;
    Pertussis pertussis;
    Pneumococcal pneumococcal;
    Polio polio;
    Rubella rubella;
    Tetanus tetanus;
    Tuberculosis tuberculosis;
    Bcg bcg;
    SixInOne sixInOne;
    Pcv pcv;
    Menc menc;
    Mmr mmr;
    Hib hib;

    private FragmentManager fragmentManager;
    private Intent intent;
    private boolean isPresent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_immunisation_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialization();
        if (intent != null){

            String name = intent.getStringExtra("name");

            if (name.contains("BCG")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerImmunisation,bcg,"tag");
                isPresent = true;
                transaction.commit();
            }
            else if (name.contains("6 in 1")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerImmunisation, sixInOne, "tag");
                isPresent = true;
                transaction.commit();
            }
            else if (name.contains("PCV")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerImmunisation, pcv, "tag");
                isPresent = true;
                transaction.commit();
            }
            else if (name.contains("MenC")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerImmunisation, menc, "tag");
                isPresent = true;
                transaction.commit();
            }
            else if (name.contains("MMR")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerImmunisation, mmr, "tag");
                isPresent = true;
                transaction.commit();
            }
            else if (name.contains("Hib")){

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.add(R.id.containerImmunisation, hib, "tag");
                isPresent = true;
                transaction.commit();
            }

        }
    }

    public void initialization(){


        fragmentManager = getSupportFragmentManager();
        intent = getIntent();
        diphtheria = new Diphtheria();
        haemophilus = new Haemophilus();
        hepatitisB = new HepatitisB();
        measles = new Measles();
        meningococcal = new Meningococcal();
        mumps = new Mumps();
        pertussis = new Pertussis();
        pneumococcal = new Pneumococcal();
        polio = new Polio();
        rubella = new Rubella();
        tetanus = new Tetanus();
        tuberculosis = new Tuberculosis();

        bcg = new Bcg();
        sixInOne = new SixInOne();
        pcv = new Pcv();
        menc = new Menc();
        mmr = new Mmr();
        hib = new Hib();

    }
}
