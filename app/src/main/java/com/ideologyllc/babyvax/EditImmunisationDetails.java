package com.ideologyllc.babyvax;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ideologyllc.babyvax.HelperClasses.AgeCalculation;
import com.ideologyllc.babyvax.database.ChildrenDb;
import com.ideologyllc.babyvax.template.Child;
import com.ideologyllc.babyvax.template.GivenImmunisationHistory;

import java.util.ArrayList;
import java.util.Calendar;

public class EditImmunisationDetails extends AppCompatActivity {

    EditText input_hse_number, input_doctor, input_lot, input_date;
    TextView textViewPharmaName, textChildName, textChildAge, textImmunisationName, textPharmaName;
    Spinner spinner;
    Button buttonSave;
    private ChildrenDb db;
    private ArrayAdapter<String> pharmaAdapter;
    private String[] pharmaList;
    private Intent intent;
    private int childImmunisationID;
    private int childId;
    private Child aChild = null;
    private String immuName;
    private String dueDate;
    private int pharmaid = -1;
    private String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    private String givenDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_immunisation_details);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        initialize();
        if (intent != null){
            Bundle b = intent.getExtras();
            childImmunisationID = b.getInt("childImmunisationID", -1);
            childId = b.getInt("id", -1);
            aChild = db.getAChild(childId);
            immuName = b.getString("immuName", "Immunisation");
            dueDate = b.getString("givenDate", "Due");
        }
        pharmaAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.custom_spinner, R.id.spnText, pharmaList);
        spinner.setAdapter(pharmaAdapter);


        input_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(1);
            }
        });

        if (db.getAGivenImmunisationHistory(childImmunisationID) != null){

            input_hse_number.setText(db.getAGivenImmunisationHistory(childImmunisationID).getHseNumber());
            input_doctor.setText(db.getAGivenImmunisationHistory(childImmunisationID).getDoctorName());
            input_lot.setText(db.getAGivenImmunisationHistory(childImmunisationID).getLot());
            input_date.setText(db.getAChildImmunisation(childImmunisationID).getGivenDate());
            int pharmaId = db.getAGivenImmunisationHistory(childImmunisationID).getPharmaID();
            if (pharmaId > 0){

                textViewPharmaName.setVisibility(View.GONE);
                textPharmaName.setText(db.getAPharma(pharmaId).getPharmaName());
            }
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                textViewPharmaName.setText(pharmaList[position]);
                if (!textViewPharmaName.getText().toString().equalsIgnoreCase("Change")){
                    textPharmaName.setVisibility(View.GONE);
                    textViewPharmaName.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        textChildAge.setText(new AgeCalculation(aChild.getDOB()).getAge());
        textChildName.setText(aChild.getChildName());
        textImmunisationName.setText(immuName);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                givenDate = input_date.getText().toString();
                if (givenDate.isEmpty()){
                    givenDate = dueDate;
                }
                pharmaid = pharmaIdFromName(textViewPharmaName.getText().toString());

                if (db.getAGivenImmunisationHistory(childImmunisationID) == null){
                    db.addGivenImmunisationHistory(new GivenImmunisationHistory(childImmunisationID,pharmaid,input_hse_number.getText().toString(),input_doctor.getText().toString(),input_lot.getText().toString()));

                }else {
                    db.updateGivenImmunisationHistory(new GivenImmunisationHistory(db.getAGivenImmunisationHistory(childImmunisationID).getId(), childImmunisationID, pharmaid, input_hse_number.getText().toString(),input_doctor.getText().toString(),input_lot.getText().toString()));
                }
                db.updateChildImmunisationGivenDate(childImmunisationID, givenDate);
                Intent i = new Intent(getApplicationContext(), ChildImmunisationHistory.class);
                i.putExtra("id", childId);
                startActivity(i);
                finish();
            }
        });

    }

    public void initialize(){
        input_hse_number = (EditText) findViewById(R.id.input_hse_number);
        input_doctor = (EditText) findViewById(R.id.input_doctor);
        input_lot = (EditText) findViewById(R.id.input_lot);
        input_date = (EditText) findViewById(R.id.input_date);
        buttonSave = (Button) findViewById(R.id.buttonSave);
        textViewPharmaName = (TextView) findViewById(R.id.textViewPharmaName);
        textChildName = (TextView) findViewById(R.id.textChildName);
        textChildAge = (TextView) findViewById(R.id.textChildAge);
        textImmunisationName = (TextView) findViewById(R.id.textImmunisationName);
        textPharmaName = (TextView) findViewById(R.id.textPharmaName);
        spinner = (Spinner) findViewById(R.id.spinner);
        db = new ChildrenDb(getApplicationContext());
        intent = getIntent();

        pharmaList = new String[db.getAllPharma().size()+1];
        pharmaList[0] = "Change";
        for (int i = 0; i < db.getAllPharma().size(); i++){
            pharmaList[i+1] = db.getAllPharma().get(i).getPharmaName();
        }
    }

    public int pharmaIdFromName(String pharmaName){

        int id;
        switch (pharmaName){

            case "SSI":
                id = 1;
                break;
            case "GSK":
                id = 2;
                break;
            case "Pfizer":
                id = 3;
                break;
            case "Novartis GSK":
                id = 4;
                break;
            case "Sanofi Pasteur":
                id = 5;
                break;
            case "MSD":
                id = 6;
                break;
            default:
                id = -1;
        }

        return id;
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        switch (id){

            case 1:

                return new DatePickerDialog(this,datePickerListener, year, month, day);

        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

            if (input_date.getText().toString() != null){

                input_date.setText(" ");
                input_date.setText(dayOfMonth+"-"+months[monthOfYear]+"-"+year);


            }else {
                input_date.setText(dayOfMonth+"-"+months[monthOfYear]+"-"+year);
            }

        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(getApplicationContext(), ChildImmunisationHistory.class);
        i.putExtra("id", childId);
        startActivity(i);
        finish();
    }
}
