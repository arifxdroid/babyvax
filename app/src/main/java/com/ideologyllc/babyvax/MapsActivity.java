package com.ideologyllc.babyvax;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ideologyllc.babyvax.HelperClasses.LocationTraker;
import com.ideologyllc.babyvax.database.FlagHelperDB;
import com.ideologyllc.babyvax.template.MyLocation;

import android.provider.Settings.Secure;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener,GoogleMap.OnMapClickListener,GoogleMap.OnMarkerDragListener {

    private GoogleMap mMap;
    private LatLng cityLocation;
    private String countryName = "";
    CameraPosition INIT;
    private Context context;
    private double latitude;
    private double longitude;
    private LocationTraker locationTraker;
    private Button btnNext;
    private String deviceID;
    ProgressDialog progressDialog;
    FlagHelperDB flagHelperDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        context = this;
        locationTraker = new LocationTraker(context);
        flagHelperDB = new FlagHelperDB(getApplicationContext());
        deviceID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);

        if (locationTraker.isGpsEnable()){

            try {

                latitude = locationTraker.getlat();
                longitude = locationTraker.getLon();
                countryName = locationTraker.getCountryName(latitude, longitude);
                flagHelperDB.addLocation(new MyLocation(Double.toString(latitude), Double.toString(longitude), countryName, deviceID));

            }catch (Exception e){
                // TODO
            }

        }else {
            showSettingsAlert();
        }

        cityLocation = new LatLng(latitude, longitude);

        mMap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.setOnMapClickListener(this);
        mMap.getUiSettings().setCompassEnabled(true);

        btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MapsActivity.this, MainHome.class);
                startActivity(i);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1) {
            switch (requestCode) {
                case 1:
                    restartActivity();
                    break;
            }
        }
    }

    //Restart Activity
    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    public void showSettingsAlert(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS is settings");
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 1);
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        progressDialog = ProgressDialog.show(MapsActivity.this, "", "Please wait...");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Message msg = Message.obtain();
                Bundle b = new Bundle();
                b.putInt("status",1);
                msg.setData(b);
                handler.sendMessage(msg);
            }
        }).start();
        //restartActivity();
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            int status = msg.getData().getInt("status");
            if ( status == 1){
                progressDialog.dismiss();
                restartActivity();
            }
        }
    };

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.addMarker( new MarkerOptions()
                .position(cityLocation)
                .title(countryName)
                .snippet("").draggable(false)).showInfoWindow();

        INIT =
                new CameraPosition.Builder()
                        .target(cityLocation)
                        .zoom(6.5F)
                        .bearing(0F) // orientation
                        .tilt( 50F) // viewing angle
                        .build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(INIT));
    }


    @Override
    public void onMapClick(LatLng latLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    @Override
    public void onMapLongClick(LatLng latLng) {

//        mMap.addMarker(new MarkerOptions()
//                .position(latLng)
//                .title(latLng.toString())
//                .draggable(true));

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker arg0) {

//        LatLng dragPosition = arg0.getPosition();
//        double dragLat = dragPosition.latitude;
//        double dragLong = dragPosition.longitude;
//
//        mMap.addMarker( new MarkerOptions()
//                .position(new LatLng(dragLat,dragLong))
//                .title("Location")
//                .snippet("First Marker")
//                .draggable(true)).showInfoWindow();
//
//        INIT =
//                new CameraPosition.Builder()
//                        .target(new LatLng(dragLat,dragLong))
//                        .zoom(6.5F)
//                        .bearing(300F) // orientation
//                        .tilt( 50F) // viewing angle
//                        .build();
//        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(INIT));
//        Toast.makeText(getApplicationContext(), "Marker Dragged..!", Toast.LENGTH_LONG).show();


    }
}
