package com.ideologyllc.babyvax;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import com.ideologyllc.babyvax.fragments.About;
import com.ideologyllc.babyvax.fragments.VaccineAndAutism;

public class MainContainer extends AppCompatActivity {

    About about;
    VaccineAndAutism vaccineAndAutism;
    FrameLayout mainContainer;
    private Intent intent;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_container);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        initialize();

        if (intent != null){
            String name = intent.getStringExtra("name");
            switch (name){
                case "About the app":
                    transaction = fragmentManager.beginTransaction();
                    transaction.add(R.id.mainContainer, about, "About the app");
                    transaction.commit();
                    break;
                case "Vaccine and autism":
                    transaction = fragmentManager.beginTransaction();
                    transaction.add(R.id.mainContainer, vaccineAndAutism, "Vaccine and autism");
                    transaction.commit();
                    break;
            }
        }
    }

    public void initialize(){
        about = new About();
        vaccineAndAutism = new VaccineAndAutism();
        fragmentManager = getSupportFragmentManager();
        intent = getIntent();
        mainContainer = (FrameLayout) findViewById(R.id.mainContainer);
    }

}
