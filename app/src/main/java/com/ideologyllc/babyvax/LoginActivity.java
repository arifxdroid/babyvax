package com.ideologyllc.babyvax;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ideologyllc.babyvax.HelperClasses.ServiceHelper;
import com.ideologyllc.babyvax.HelperClasses.SessionHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    Button buttonToRegisterScreen;
    Button btnLogin;
    EditText etUsername, etPassword;
    private ProgressDialog pDialog;
    private String username;
    private String password;
    private SessionHelper session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initialize();



        buttonToRegisterScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = etUsername.getText().toString();
                password = etPassword.getText().toString();

                if (!username.isEmpty() && !password.isEmpty()){
                    verifyLogin(username, password);
                }else Toast.makeText(getApplicationContext(), "Please enter the login credential", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void initialize(){
        buttonToRegisterScreen = (Button) findViewById(R.id.buttonToRegisterScreen);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        pDialog = new ProgressDialog(this);
        session = new SessionHelper(getApplicationContext());
    }

    public void verifyLogin(String username, String password){

        String url =  "http://express-agency.somee.com/test/api/babyvaxapi/verify/"+username+"/"+password+"/";
        pDialog.setMessage("Logging in ...");
        pDialog.show();
        StringRequest strRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                String value = response.toString();
                if (!value.isEmpty()){
                    if (value.equalsIgnoreCase("true")){

                        pDialog.dismiss();
                        session.setLogin(true);
                        String name = "My family";
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        i.putExtra("name", name);
                        startActivity(i);
                        finish();

                    }else if (value.equalsIgnoreCase("false")){
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Incorrect username or password", Toast.LENGTH_LONG).show();
                    }
                    else {
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "Login failed", Toast.LENGTH_LONG).show();
                pDialog.dismiss();
            }
        });
        ServiceHelper.getInstance().addToRequestQueue(strRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        i.putExtra("name", "Vaccination schedule");
        startActivity(i);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
    }
}
